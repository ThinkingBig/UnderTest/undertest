# Multiple Feature File Source Folders  

UnderTests is a convention based framework.  One of those conventions is that there is a `Features` folder inside of the acceptance test project, and that folder contains the source feature files for tests.

The contents of the Feature are copied, by the compiler, to the `bin` folder for processing.

## Adding other folders

Individuals raised scenarios where feature files for a project are stored outside of the convention folder.  We can support this with some updates.

### Steps

1. Open your UnderTest `csproj` file.
2. Locate the feature copy section it should look like.
    ```xml
     <!-- our feature files for execution -->
     <ItemGroup>
       <None Update="$(ProjectDir)Features\**" CopyToOutputDirectory="Always" />
     </ItemGroup>
    ``` 
3. Copy this section and paste it below in the same file.
4. Change the PATH section of `$(ProjectDir)PATH\**` to point to the additional target folder.
5. Add an attribute of `LinkBase="Features"`
6. Save.
7. Develop like the feature files are in your project.

The resulting `ItemGroup` should look something like

```
  <!-- our feature files for execution -->
  <ItemGroup>
    <Content Include="$(ProjectDir)..\..\library\Features\**\*.*" CopyToOutputDirectory="Always" LinkBase="Features" />
  </ItemGroup>
```

***note:*** we recommend using [submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules) if the feature files cannot be in your project repo.
