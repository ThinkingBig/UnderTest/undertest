using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using GlobExpressions;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.NuGet;
using Nuke.Common.Tools.NUnit;
using Nuke.Common.Tools.Slack;
using Nuke.Common.Utilities;
using Nuke.Common.Utilities.Collections;
using UnderTest.Nuke;
using static Nuke.Common.ChangeLog.ChangelogTasks;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.Git.GitTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;
using static Nuke.Common.Tools.NuGet.NuGetTasks;
using static Nuke.Common.Tools.NUnit.NUnitTasks;
using static Nuke.Common.Tools.Slack.SlackTasks;

[assembly: ExcludeFromCodeCoverageAttribute]
public class Build : NukeBuild
{
  [SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1201:ElementsMustAppearInTheCorrectOrder", Justification = "Reviewed. Suppression is OK here.")]
  public static int Main()
  {
    return Execute<Build>(x => x.Pack);
  }

  [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
  public readonly string Configuration = IsLocalBuild ? "Debug" : "Release";

  [Parameter("Api Key for Nuget.org when pushing our nuget package")]
  public readonly string NugetOrgApiKey;

  [Solution(@"src/UnderTest.sln")]
  public readonly Solution Solution;

  [Parameter("Slack webhook")]
  public readonly string SlackWebhook;

  public AbsolutePath SourceDirectory => RootDirectory / "src";

  public AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";

  public AbsolutePath TestDirectory => SourceDirectory / "UnderTest.Tests";

  public List<AbsolutePath> PackableProjects => new List<AbsolutePath>
  {
    SourceDirectory / "UnderTest/UnderTest.csproj",
    SourceDirectory / "UnderTest.Support/UnderTest.Support.csproj"
  };

  string ChangelogFile => RootDirectory / "CHANGELOG.md";

  IEnumerable<string> ChangelogSectionNotes => ExtractChangelogSectionNotes(ChangelogFile);

  public Target Clean =>
    _ =>
      _.Executes(() =>
      {
        foreach (var directory in GlobDirectories(SourceDirectory, "**/bin", "**/obj"))
        {
          DeleteDirectory(directory);
        }
        EnsureCleanDirectory(ArtifactsDirectory);
      });

  public Target Restore => _ =>
    _.DependsOn(Clean)
      .Executes(() =>
      {
        DotNetRestore(s => s
          .SetWorkingDirectory(SourceDirectory)
          .SetProjectFile(Solution));
      });

  public Target Compile =>
    _ =>
      _.DependsOn(Restore)
        .Executes(() =>
        {
          DotNetBuild(s => s
            .SetProjectFile(Solution)
            .SetWorkingDirectory(SourceDirectory)
            .EnableNoRestore()
            .SetConfiguration(Configuration));
        });

  public Target UnitTests =>
    _ =>
      _.DependsOn(Compile)
        .Executes(() =>
        {
          var files = GlobFiles(TestDirectory, $"**bin/{Configuration}/**/net472/*.Tests.dll")
                        .NotEmpty();
          NUnit3(s => s
            .AddInputFiles(files));

          foreach (var project in Solution.GetProjects("*.Tests"))
          {
            DotNetTest(s => s
              .SetConfiguration(Configuration)
              .EnableNoBuild()
              .SetLogger("trx")
              .SetProjectFile(project));
          }
        });

  public Target Examples =>
    _ => _
      .DependsOn(UnitTests)
      .Executes(() =>
      {
        foreach (var project in Solution.GetProjects("UnderTest.Examples.*"))
        {
          Logger.Info("Executing example project: " + project.Path);
          Glob.Files(Path.GetDirectoryName(project.Path), $"bin/**/{Configuration}/**/{ Path.GetFileNameWithoutExtension(project.Path)}.dll")
            .ForEach(x => UnderTestTasks.UnderTest(y => y
              .SetProjectFile(Path.Combine(Path.GetDirectoryName(project.Path), x))));
        }
      });

  public Target Pack =>
    _ => _
      .DependsOn(Examples)
      .Executes(() =>
      {
        DotNetPack(s => s
          .EnableNoBuild()
          .SetConfiguration(Configuration)
          .EnableIncludeSymbols()
          .SetSymbolPackageFormat(DotNetSymbolPackageFormat.snupkg)
          .SetOutputDirectory(ArtifactsDirectory)
          .CombineWith(PackableProjects, (settings, path)
            => settings.SetProject(path)));
      });

  Target Push =>
    _ =>
      _.DependsOn(Pack)
        .OnlyWhenDynamic(() => OnABranchWeWantToPushToNugetOrg())
        .Requires(() => NugetOrgApiKey)
        .Requires(() => GitHasCleanWorkingCopy())
        .Requires(() => IsReleaseConfiguration())
        .Executes(() =>
        {
          GlobFiles(ArtifactsDirectory, "*.nupkg")
            .ForEach(x =>
            {
              DotNetNuGetPush(s => s
                .SetTargetPath(x)
                .SetSource("https://www.nuget.org/api/v2/package")
                .SetApiKey(NugetOrgApiKey)
                .SetLogOutput(true)
                .SetSkipDuplicate(true));
            });
        });

  public bool IsReleaseConfiguration()
  {
    return Configuration.EqualsOrdinalIgnoreCase("Release");
  }

  // our branching strategy is stable for production, release* for releases and hotfix* for hotfix items.
  public bool OnABranchWeWantToPushToNugetOrg()
  {
    var branch = this.GetCurrentBranch().ToLowerInvariant();
    Logger.Log(LogLevel.Normal, $"Current branch {branch}");

     return branch == "stable"
              || branch.StartsWith("release")
              || branch.StartsWith("hotfix");
  }

  string GetCurrentBranch()
  {
    var environmentVariable = Environment.GetEnvironmentVariable("APPVEYOR_REPO_BRANCH");
    if (!string.IsNullOrEmpty(environmentVariable))
    {
      return environmentVariable;
    }

    return Git("rev-parse --abbrev-ref HEAD", null, outputFilter: null).Select(x => x.Text).Single();
  }

}
