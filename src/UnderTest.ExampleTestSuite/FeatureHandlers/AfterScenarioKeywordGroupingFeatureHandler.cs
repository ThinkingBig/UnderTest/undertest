using UnderTest.Arguments;
using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("AfterScenarioKeywordGrouping.feature")]
  public class AfterScenarioKeywordGroupingFeatureHandler : FeatureHandler
  {
    public override void BeforeScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    {
      Log.Information("BeforeScenarioKeywordGrouping for fun");

      base.BeforeScenarioKeywordGrouping(context);
    }

    public override void BeforeBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    {
      Log.Information("BeforeBackgroundKeywordGrouping for fun");

      base.BeforeScenarioKeywordGrouping(context);
    }

    public override void AfterBackgroundKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    {
      Log.Information("AfterBackgroundKeywordGrouping for fun");

      base.AfterScenarioKeywordGrouping(context);
    }

    public override void AfterScenarioKeywordGrouping(BeforeAfterKeywordGroupingContext context)
    {
      Log.Information("AfterScenarioKeywordGrouping for fun");

      base.AfterScenarioKeywordGrouping(context);
    }

    [Given("something cool")]
    public void SomethingCool() => Noop();

    [Given(@"a user with the following information:")]
    public void GivenAUserWithTheFollowingInformation(GherkinDataTable userProfile)
    { }

    [Given(@"the user has not registered")]
    public void GivenTheUserHasNotRegistered() => Noop();

    [When(@"the user registers a new account")]
    public void WhenTheUserRegistersANewAccount() => Noop();

    [Then(@"a new account is created for the user")]
    public void ThenTheUsersIsCreated() => Noop();

    [Then(@"a confirmation email is sent to the user's email")]
    public void ThenAConfirmationEmailIsSentToTheUsersEmail() => Noop();
  }
}
