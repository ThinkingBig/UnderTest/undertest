using UnderTest.Attributes;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [HandlesFeature("Outlines/ExampleExternalData.feature")]
  public class ExampleExternalDataHandler : FeatureHandler
  {

    [Given("a user named <user-name>")]
    public void AUsername(string username) => Noop();

    [Given("the user is age <age>")]
    public void TheUserIsAge(int age) => Noop();

    [When("the user goes to their profile page")]
    public void ThenUserGoesToTheirProfilePage() => Noop();

    [Then("they see their name and age")]
    public void TheySeeTheirNameAndAge() => Noop();
  }
}
