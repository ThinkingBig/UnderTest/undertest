using JetBrains.Annotations;
using UnderTest.Attributes;
using UnderTest.Exceptions;

namespace UnderTest.ExampleTestSuite.FeatureHandlers
{
  [PublicAPI]
  [HandlesFeature("Tagging/WipExceptionThrownWithinScenarioStep.feature")]
  public class WipStepWithinScenarioFeatureHandler : FeatureHandler
  {
    [Given("Something exists")]
    public void SomethingExists() => Noop();

    [When("Something happens")]
    public void SomethingHappens()
    {
      throw new WorkInProgressScenarioException("We haven't built this yet.")
      {
        StepKeyword = "Given",
        StepText = "Something happens"
      };
    }

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
