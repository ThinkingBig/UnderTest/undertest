Feature: Simple feature with a background

  Background: this is a background
    Given something happens in the background

  Scenario: simple scenario
    Given Something exists
    When Something happens
    Then This should now be true

