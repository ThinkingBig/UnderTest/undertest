@skip-this-one
Feature: Test filter example feature

  Scenario: This feature should be filtered out when the suite is run
    Given Something exists
    When Something happens
    Then This should now be true
