Feature: Feature with base handler

  Scenario Outline: Try parameters with examples
    Given the number <number> from this step
    When the number <number-to-add> is added
    Then the result should be <result>

    Examples: Numbers and results
      | number | number-to-add | result |
      | 1      | 0             | 1      |
      | 0      | 1             | 1      |
      | 5      | 10            | 15     |
