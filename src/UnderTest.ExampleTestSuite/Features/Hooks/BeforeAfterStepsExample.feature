Feature: Example showing before and after step bindings

  Scenario: Scenario that has before and after steps wired
    Given Something is true
    When Something happens
    Then This should now be true
