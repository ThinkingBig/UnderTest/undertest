Feature: example data source

  Scenario Outline: User profile
    Given a user named <user-name>
      And the user is age <age>
    When the user goes to their profile page
    Then they see their name and age

    @ExampleData:data\ExampleExternalData-user_profile_examples.csv
    Examples: User profile data
    | user-name | age |

  Scenario Outline: Invalid data
    Given a user named <user-name>
    And the user is age <age>
    When the user goes to their profile page
    Then they see their name and age

  @ExampleData:data\ExampleExternalData-user_profile_examples_invalid.xlsx
    Examples: User profile data
      | user-name | age |

  Scenario Outline: User profile external data and example rows
    Given a user named <user-name>
    And the user is age <age>
    When the user goes to their profile page
    Then they see their name and age

    @ExampleData:data\ExampleExternalData-user_profile_examples.csv
    Examples: User profile data with rows
      | user-name | age |
      | rhood     | 20  |
      | thefriar  | 24  |
