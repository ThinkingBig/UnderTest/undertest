Feature: Scenario outline example

  Scenario Outline: simple scenario with an outline
    Given Something exists for <name>
    When Something happens
    Then This should now be true
    Examples: Test example
      | name  |
      | Rick  |
      | Morty |
    @future-version
    Examples: Test example2
      | name  |
      | Rick2 |
      | Morty2|

