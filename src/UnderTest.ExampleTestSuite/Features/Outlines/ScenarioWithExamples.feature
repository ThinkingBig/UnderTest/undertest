Feature: Examples on Scenarios

  Scenario Outline: Contains an Examples table
    Given the number <number> from this step
    When the number <number-to-add> is added
    Then the result should be <result>

    Examples: Numbers and results
      | number | number-to-add | result |
      | 0      | 0             | 0      |
      | 1      | 0             | 1      |
      | 0      | 1             | 1      |
      | 5      | 10            | 15     |
