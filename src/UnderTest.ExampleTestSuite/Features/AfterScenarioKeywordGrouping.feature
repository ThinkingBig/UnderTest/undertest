Feature: Before After Step

Background: for fun
  Given something cool

  Scenario: New user registers a new account
    Given a user with the following information:
      | Name    | Email                    |
      | Charlie | charlierulez@example.com |
      And the user has not registered
    When the user registers a new account
    Then a new account is created for the user
      And a confirmation email is sent to the user's email
