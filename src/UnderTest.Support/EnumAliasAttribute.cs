﻿using System;
using JetBrains.Annotations;

namespace UnderTest.Support
{
  [PublicAPI]
  [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
  public class EnumAliasAttribute: Attribute
  {
    public string Alias { get; }

    public EnumAliasAttribute(string alias)
    {
      Alias = alias ?? throw new ArgumentNullException(nameof(alias));
    }
  }
}
