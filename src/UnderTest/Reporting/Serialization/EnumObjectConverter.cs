using System;
using System.Globalization;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace UnderTest.Reporting.Serialization
{
  [PublicAPI]
  public class EnumObjectConverter<TEnumType> : JsonConverter where TEnumType : struct, IConvertible
  {
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
      var enumValue = (TEnumType)value;

      writer.WriteStartObject();
      writer.WritePropertyName("ID");
      writer.WriteValue(Convert.ToInt32(enumValue));
      writer.WritePropertyName("Name");
      writer.WriteValue(enumValue.ToString(CultureInfo.InvariantCulture));
      writer.WriteEndObject();
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
      if (reader.TokenType == JsonToken.Null)
      {
        return existingValue;
      }

      var child = JObject.Load(reader);
      if (child == null || !child.ContainsKey("ID"))
      {
        return existingValue;
      }

      return Enum.Parse(typeof(TEnumType), child["ID"].ToString(), true);
    }

    public override bool CanConvert(Type objectType)
    {
      return objectType == typeof(JObject);
    }
  }
}
