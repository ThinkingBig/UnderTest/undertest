﻿using JetBrains.Annotations;
using Newtonsoft.Json;

namespace UnderTest.Reporting.Serialization
{
  [PublicAPI]
  public class JsonSerializerSettingsFactory
  {
    // we set TypeNameHandling to All so we can map behavior types back in successfully.
    // this means when we read these types back in anonymously, we need to verify the types using a https://www.newtonsoft.com/json/help/html/SerializeSerializationBinder.htm
    // todo: the above is not done.
    public JsonSerializerSettings GetReportSerializerInstance()
    {
      return new JsonSerializerSettings
      {
        TypeNameHandling = TypeNameHandling.All
      };
    }
  }
}
