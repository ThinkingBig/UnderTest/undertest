using JetBrains.Annotations;

namespace UnderTest.Reporting
{
  [PublicAPI]
  public enum ScenarioRunResultType
  {
    Inconclusive = 0,
    Pass = 1,
    Failed = 2,
    Ignored = 3,
    Wip = 4,
    Mixed = 5
  }
}
