namespace UnderTest.Reporting
{
  public class TestRunScenarioResultList
  {
    public int Passed { get; set; }

    public int Failed { get; set; }

    public int Inconclusive { get; set; }

    public int Skipped { get; set; }

    public int Wip { get; set; }

    public int Total { get; set; }
  }
}
