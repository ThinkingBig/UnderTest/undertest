using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;
using UnderTest.Infrastructure;

namespace UnderTest.Reporting
{
  [PublicAPI]
  [Serializable]
  public class TestRunFeatureStrategyResult
  {
    public string StrategyName { get; set; }

    public TimeSpan Duration { get; set; }

    public DateTime EndTime { get; set; }

    public IList<ScenarioRunResult> ScenarioTestResults { get; set; } = new List<ScenarioRunResult>();

    public DateTime StartTime { get; set; }

    public void SetAndNoOpWithStrategyName(string testStrategyName)
    {
      StrategyName = testStrategyName;

      Duration = TimeSpan.Zero;
      StartTime = DateTime.UtcNow;
      EndTime = DateTime.UtcNow;
    }

    public void AddFeatureScenariosAsSkipped(FeatureContext featureContext)
    {
      ProcessFeatureChildren(featureContext.Document.Feature.Children);
    }

    private void ProcessFeatureChildren(IEnumerable<IHasLocation> children, List<StepForProcessing> backgroundSteps = null)
    {
      backgroundSteps = backgroundSteps ?? new List<StepForProcessing>();
      foreach (var child in children)
      {
        switch (child)
        {
          case Background background:
            backgroundSteps.AddRange(new List<StepForProcessing>(background.Steps.ToStepsForProcessingList()));
            break;
          case Rule rule:
            ProcessFeatureChildren(rule.Children, backgroundSteps);
            break;
          default:
            MarkScenarioAsIgnored(child);
            break;
        }
      }
    }

    private void MarkScenarioAsIgnored(IHasLocation child)
    {
      var scenario = (Scenario) child;

      var steps = scenario
        .Steps
        .ToStepsForProcessingList()
        .Select(x => new ScenarioStepResult
        {
          ScenarioStepResultType = ScenarioStepResultType.Skipped
        }.LoadStep(x)).ToList();

      steps.AddRange(scenario.Steps.Select(x => new ScenarioStepResult
      {
        ScenarioStepResultType = ScenarioStepResultType.Skipped
      }.LoadStep(new StepForProcessing(x))).ToList());

      ScenarioTestResults.Add(new ScenarioRunResult
      {
        ScenarioName = scenario.Name,
        Result = ScenarioRunResultType.Ignored,
        Keyword = scenario.Keyword,
        Description = scenario.Description,
        StepResults = steps
      });
    }
  }
}
