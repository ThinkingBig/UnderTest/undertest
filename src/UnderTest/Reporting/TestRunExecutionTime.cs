using System;
using JetBrains.Annotations;

namespace UnderTest.Reporting
{
  [PublicAPI]
  [Serializable]
  public class TestRunExecutionTime
  {
    public TestRunExecutionTime()
    {
      StartTime = DateTime.UtcNow;
    }

    public DateTime StartTime { get; set; }

    public DateTime EndTime { get; set; }

    public TimeSpan Duration => EndTime - StartTime;

    public override string ToString()
    {
      var result = string.Empty;
      var duration = Duration;

      if (duration.Days > 0)
      {
        return $"{duration.Days} {SingleOrPlural(duration.Days, "day")}, {duration.Hours} {SingleOrPlural(duration.Hours, "hour")}, {duration.Minutes} minutes, {duration.Seconds}.{duration.Milliseconds} seconds";
      }

      if (duration.Hours > 0)
      {
        return $"{duration.Hours} {SingleOrPlural(duration.Hours, "hour")}, {duration.Minutes} {SingleOrPlural(duration.Minutes, "minute")}, {duration.Seconds}.{duration.Milliseconds} seconds";
      }

      if (duration.Minutes > 0)
      {
        return $"{duration.Minutes} {SingleOrPlural(duration.Minutes, "minute")}, {duration.Seconds}.{duration.Milliseconds} seconds";
      }

      return $"{duration.Seconds}.{duration.Milliseconds} seconds";
    }

    private string SingleOrPlural(int value, string single, string pluralAddition = "s")
    {
      if (value <= 1)
      {
        return single;
      }

      return single + pluralAddition;
    }
  }
}
