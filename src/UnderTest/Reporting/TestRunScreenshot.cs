using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace UnderTest.Reporting
{
  [PublicAPI]
  [Serializable]
  public class TestRunScreenshot
  {
    public string FilePath { get; set; }

    public string Id { get; set; }

    public DateTime Timestamp { get; set; }

    public Dictionary<string, object> AdditionalContext { get; set; } = new Dictionary<string, object>();
  }
}
