using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;
using UnderTest.Exceptions;
using UnderTest.Infrastructure;
using UnderTest.Infrastructure.Binding;
using DataTable = Gherkin.Ast.DataTable;

namespace UnderTest.Arguments
{
  [PublicAPI]
  public class GherkinDataTable
  {
    public GherkinDataTable(DataTable source)
    {
      if (!source.Rows.Any())
      {
        throw new ArgumentException("DataTables must have at least one header row");
      }

      Header = HeaderFromRows(source);
      Rows = ExactDataRowsFromOverallRows(source);
    }

    public GherkinDataTable(DataTable source, ExampleTableRow exampleTableRow)
    {
      Header = HeaderFromRows(source);
      Rows = ExactDataRowsFromOverallRows(source)
        .Select(row =>
          new GherkinDataTableRow(Header, row
            .Cells
            .Select(col => new GherkinDataTableCell(exampleTableRow?.ApplyRowToStepText(col.Value) ?? col.Value))
            .ToList()))
        .ToList();
    }

    public GherkinDataTableHeader Header { get; private set; }

    public IList<GherkinDataTableRow> Rows { get; private set; }

    public T GenerateInstance<T>() where T : new()
    {
      if (Header == null)
      {
        throw new Exception("GenerateInstance cannot be called on a table with no header.");
      }

      if (IsFullMatchingColumnBasedDataTable<T>())
      {
        return ConvertColumnBasedDataTableToInstance<T>();
      }

      if (Rows.Count() == 1)
      {
        var row = Rows.First();

        return ConvertRowToInstance<T>(row);
      }

      if (IsPartiallyMatchingColumnBasedDataTable<T>())
      {
        return ConvertColumnBasedDataTableToInstance<T>();
      }

      throw new Exception("GenerateInstance must be called on a table with with one row or two columns.");
    }

    public IList<T> GenerateList<T>() where T : new()
    {
      if (Header == null)
      {
        throw new Exception("GenerateInstance called on a table with no header.");
      }

      return Rows.Select(ConvertRowToInstance<T>).ToList();
    }

    public override string ToString()
    {
      var columnWidths = Header
        .Cells
        .Select((x, index) => Math.Max(x.Value.Length, Rows.Max(row => row[index].Value.Length)) )
        .ToList();

      string BuildRow(GherkinDataTableRow row)
      {
        var columnIndex = 0;

        return $"{row.Cells.Aggregate("|", (current, cell) => current + $" {cell.Value.PadRight(columnWidths[columnIndex++])} |")}";
      }

      var colIndex = 0;
      var result = $"{Header.Cells.Aggregate("|", (current, cell) => current + $" {cell.Value.PadRight(columnWidths[colIndex++])} |")}\n";

      return Rows.Aggregate(result, (current, row) => current + $"{BuildRow(row)}\n");
    }

    // todo - this should likely be moved out to a separate concern
    private T ConvertRowToInstance<T>(GherkinDataTableRow row) where T : new()
    {
      var result = new T();

      var col = 0;
      foreach (var variableCell in Header.Cells)
      {
        var valueCell = row.Cells.ElementAt(col++);
        var header = MakeHeaderPropertyNameSafe(variableCell.Value);
        var value = valueCell.Value;

        var prop = GetPropertyFromFieldName<T>(header);

        SetPropOnObjectSupportingNull(value, prop, result);
      }

      return result;
    }

    private T ConvertColumnBasedDataTableToInstance<T>() where T : new()
    {
      var result = new T();

      foreach (var row in Rows)
      {
        var header = MakeHeaderPropertyNameSafe(row.Cells.First().Value);
        var value = row.Cells.Skip(1).First().Value;

        var prop = GetPropertyFromFieldName<T>(header);

        SetPropOnObjectSupportingNull(value, prop, result);
      }

      return result;
    }

    private static PropertyInfo GetPropertyFromFieldName<T>(string header) where T : new()
    {
      var prop = typeof(T).GetProperty(header,
        BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty);
      if (prop == null)
      {
        throw new InvalidExampleTablePropertyNameException($"Unable to locate header name {header}")
        {
          HeaderName = header
        };
      }

      return prop;
    }

    private IList<GherkinDataTableRow> ExactDataRowsFromOverallRows(DataTable source)
    {
      return source.Rows
        .Skip(1)
        .Select(row =>
          new GherkinDataTableRow(Header, row
            .Cells
            .Select(col => new GherkinDataTableCell(col.Value))
            .ToList()))
        .ToList();
    }

    private bool IsPartiallyMatchingColumnBasedDataTable<T>() where T : new()
    {
      if (Header.Cells.Count != 2)
      {
        return false;
      }

      try
      {
        return Rows.Any(row =>
        {
          try
          {
            return GetPropertyFromFieldName<T>(MakeHeaderPropertyNameSafe(row.Cells.First().Value)) != null;
          }
          catch (InvalidExampleTablePropertyNameException)
          {
            return false;
          }
        });
      }
      catch (Exception)
      {
        return false;
      }
    }

    private bool IsFullMatchingColumnBasedDataTable<T>() where T : new()
    {
      if (Header.Cells.Count != 2)
      {
        return false;
      }

      try
      {
        return Rows.All(row => GetPropertyFromFieldName<T>(MakeHeaderPropertyNameSafe(row.Cells.First().Value)) != null);
      }
      catch (Exception)
      {
        return false;
      }
    }

    private static GherkinDataTableHeader HeaderFromRows(DataTable source)
    {
      return source.Rows
        .Take(1)
        .Select(row =>
          new GherkinDataTableHeader(row
            .Cells
            .Select(col => new GherkinDataTableCell(col.Value)).ToList()))
        .First();
    }

    private static void SetPropOnObjectSupportingNull<T>(string value, PropertyInfo prop, T result) where T : new()
    {
      // support explicit nulls
      if (string.Equals(value, "null", StringComparison.CurrentCultureIgnoreCase))
      {
        value = null;
      }

      prop.SetValue(result, value, BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase,
        new UnderTestInvokeBinder(), null, CultureInfo.CurrentCulture);
    }

    // todo - move this to an injected type to allow our users to override this behavior
    //        this does not current cover off the spec and this will likely become much more complex
    private string MakeHeaderPropertyNameSafe(string variableCellValue)
    {
      return variableCellValue
        .Replace(" ", string.Empty)
        .Replace("-", string.Empty);
    }
  }
}
