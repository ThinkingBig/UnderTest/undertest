using JetBrains.Annotations;

namespace UnderTest.Arguments
{
  [PublicAPI]
  public class GherkinDataTableCell
  {
    public GherkinDataTableCell(string value)
    {
      Value = value;
    }

    public string Value { get; set; }

    public override string ToString() => Value;
  }
}
