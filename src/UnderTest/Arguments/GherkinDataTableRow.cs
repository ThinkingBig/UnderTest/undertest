using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace UnderTest.Arguments
{
  [PublicAPI]
  public class GherkinDataTableRow
  {
    public GherkinDataTableRow(GherkinDataTableHeader header, IList<GherkinDataTableCell> cells)
    {
      Header = header;
      Cells = cells;

      headerIndex = header.Cells
        .Select((cell, index) => new { cell, index })
        .ToDictionary(x => x.cell.Value, x => x.index);
    }

    public IList<GherkinDataTableCell> Cells { get; private set; }

    public GherkinDataTableHeader Header { get; private set; }

    private Dictionary<string, int> headerIndex;

    public GherkinDataTableCell this[string headerP]
    {
      get
      {
        var index = headerIndex[headerP];

        return Cells[index];
      }
    }

    public GherkinDataTableCell this[int index] => Cells[index];

    public override string ToString() => string.Join("|", Cells);

    public bool ContainsKey(string headerP)
    {
      return headerIndex.ContainsKey(headerP);
    }
  }
}
