namespace UnderTest.Locators
{
  public interface IFeatureHandlerLocator
  {
    string Name { get; }

    TestRunSettings TestSettings { get; }

    IFeatureHandler LocateContainerByFeatureContext(FeatureContext feature);
  }
}
