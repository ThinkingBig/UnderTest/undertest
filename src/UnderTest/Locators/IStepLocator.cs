using JetBrains.Annotations;
using UnderTest.Infrastructure;

namespace UnderTest.Locators
{
  [PublicAPI]
  public interface IStepLocator
  {
    string Name { get; }

    TestRunSettings TestSettings { get; }

    StepLocatorMatch LocateStep(StepForProcessing step, IFeatureHandler handler);
  }
}
