using JetBrains.Annotations;
using Serilog;
using UnderTest.Attributes;
using UnderTest.Infrastructure;

namespace UnderTest
{
  [PublicAPI]
  public abstract class FeatureHandlerServiceBase : IHasLogger
  {
    [PropertyInjected]
    public ILogger Log { get; set; }

    public void OnInit()
    { }
  }
}
