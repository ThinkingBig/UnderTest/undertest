using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class MissingStepBindingException : UnderTestException
  {
    public string StepKeyword { get; set; }
    public string StepText { get; set; }

    public MissingStepBindingException(string message)
      : base(message)
    { }

    public MissingStepBindingException(string message, Exception inner)
      : base(message, inner)
    { }

    protected MissingStepBindingException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      StepKeyword = info.GetString("StepKeyword");
      StepText = info.GetString("StepText");
    }

    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue("StepKeyword", StepKeyword);
      info.AddValue("StepText", StepText);

      base.GetObjectData(info, context);
    }
  }
}
