using System;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class NoFeatureAssembliesConfiguredException : UnderTestException
  {
    public NoFeatureAssembliesConfiguredException()
    { }

    public NoFeatureAssembliesConfiguredException(string message)
      : base(message)
    { }
  }
}
