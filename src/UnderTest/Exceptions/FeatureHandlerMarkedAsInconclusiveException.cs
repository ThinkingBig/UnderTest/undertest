﻿using System;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class FeatureHandlerMarkedAsInconclusiveException : UnderTestException
  {
    public IFeatureHandler Handler { get; set; }

    public FeatureHandlerMarkedAsInconclusiveException(string message)
      : base(message)
    { }

    public FeatureHandlerMarkedAsInconclusiveException(string message, Exception inner)
      : base(message, inner)
    { }
  }
}
