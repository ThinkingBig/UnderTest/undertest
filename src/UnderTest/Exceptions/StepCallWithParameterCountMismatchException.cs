﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace UnderTest.Exceptions
{
  [Serializable]
  public class StepCallWithParameterCountMismatchException : UnderTestException
  {
    public string MethodName { get; set; }
    public int IntendedParameterCount { get; set; }
    public string MethodCallSignature { get; set; }

    public StepCallWithParameterCountMismatchException(string message)
      : base(message)
    { }

    public StepCallWithParameterCountMismatchException(string message, Exception inner)
      : base(message, inner)
    { }

    protected StepCallWithParameterCountMismatchException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      MethodName = info.GetString("MethodName");
      IntendedParameterCount = info.GetInt32("IntendedParameterCount");
      MethodCallSignature = info.GetString("MethodCallSignature");
    }

    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue("MethodName", MethodName);
      info.AddValue("IntendedParameterCount", IntendedParameterCount);
      info.AddValue("MethodCallSignature", MethodCallSignature);

      base.GetObjectData(info, context);
    }
  }
}
