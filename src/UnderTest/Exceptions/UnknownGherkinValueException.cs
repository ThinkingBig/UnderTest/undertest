﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class UnknownGherkinValueException : UnderTestException
  {
    public string Value { get; set; }

    public UnknownGherkinValueException(string message)
      : base(message)
    { }

    public UnknownGherkinValueException(string message, Exception inner)
      : base(message, inner)
    { }

    public UnknownGherkinValueException(string message, string value)
      : base(message)
    {
      Value = value;
    }

    public UnknownGherkinValueException(string message, Exception inner, string value)
      : base(message, inner)
    {
      Value = value;
    }

    protected UnknownGherkinValueException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      Value = info.GetString(nameof(Value));
    }

    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue(nameof(Value), Value);

      base.GetObjectData(info, context);
    }
  }
}
