using System;
using System.Reflection;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class MethodMarkedAsInconclusiveException : UnderTestException
  {
    public MethodInfo Method { get; set; }

    public MethodMarkedAsInconclusiveException(string message)
      : base(message)
    { }

    public MethodMarkedAsInconclusiveException(string message, Exception inner)
      : base(message, inner)
    { }
  }
}
