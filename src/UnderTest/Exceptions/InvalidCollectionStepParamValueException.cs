using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class InvalidCollectionStepParamValueException : UnderTestException
  {
    public string ListCollectionTypeName { get; set; }
    public string Value { get; set; }

    public InvalidCollectionStepParamValueException(string message)
      : base(message)
    { }

    public InvalidCollectionStepParamValueException(string message, Exception inner)
      : base(message, inner)
    { }

    protected InvalidCollectionStepParamValueException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      ListCollectionTypeName = info.GetString("ListCollectionTypeName");
      Value = info.GetString("Value");
    }

    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue("ListCollectionTypeName", ListCollectionTypeName);
      info.AddValue("Value", Value);

      base.GetObjectData(info, context);
    }
  }
}
