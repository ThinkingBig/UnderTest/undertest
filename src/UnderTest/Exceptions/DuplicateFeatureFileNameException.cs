using System;
using System.Runtime.Serialization;
using System.Security.Permissions;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class DuplicateFeatureFileNameException : UnderTestException
  {
    public string FeatureFileName { get; set; }

    public DuplicateFeatureFileNameException(string message)
      : base(message)
    { }

    public DuplicateFeatureFileNameException(string message, Exception inner)
      : base(message, inner)
    { }

    protected DuplicateFeatureFileNameException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      FeatureFileName = info.GetString("FeatureFileName");
    }

    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue("FeatureFileName", FeatureFileName);

      base.GetObjectData(info, context);
    }
  }
}
