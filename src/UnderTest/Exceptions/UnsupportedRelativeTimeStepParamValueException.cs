using System;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class UnsupportedRelativeTimeStepParamValueException : UnderTestException
  {
    public UnsupportedRelativeTimeStepParamValueException()
    { }

    public UnsupportedRelativeTimeStepParamValueException(string message)
      : base(message)
    { }
  }
}
