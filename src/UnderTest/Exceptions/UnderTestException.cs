using System;
using System.Runtime.Serialization;

namespace UnderTest.Exceptions
{
  public class UnderTestException: Exception
  {
    protected UnderTestException()
    { }

    protected UnderTestException(string message)
        : base(message)
    { }

    protected UnderTestException(string message, Exception inner)
        : base(message, inner)
    { }

    protected UnderTestException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    { }
  }
}
