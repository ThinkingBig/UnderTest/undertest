using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Security.Permissions;
using CommandLine;
using JetBrains.Annotations;

namespace UnderTest.Exceptions
{
  [PublicAPI]
  [Serializable]
  public class FailedToParseCommandLineParametersUnderTestException : UnderTestException
  {
    public FailedToParseCommandLineParametersUnderTestException(string message)
      : base(message)
    { }

    public FailedToParseCommandLineParametersUnderTestException(string message, Exception inner)
      : base(message, inner)
    { }

    public IEnumerable<Error> Errors;

    protected FailedToParseCommandLineParametersUnderTestException(SerializationInfo info, StreamingContext context)
      : base(info, context)
    {
      Errors = (IEnumerable<Error>)info.GetValue("Errors", typeof(IEnumerable<Error>));
    }

    [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
    public override void GetObjectData(SerializationInfo info, StreamingContext context)
    {
      if (info == null)
      {
        throw new ArgumentNullException(nameof(info));
      }

      info.AddValue("Errors", Errors);

      base.GetObjectData(info, context);
    }
  }
}
