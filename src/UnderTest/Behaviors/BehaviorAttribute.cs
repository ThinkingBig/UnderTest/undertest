﻿using System;

namespace UnderTest.Behaviors
{
  [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
  public class BehaviorAttribute : Attribute
  {
    public virtual void OnStepExecuting(StepExecutingContext context)
    { }
    
    public virtual void OnStepExecuted(StepExecutingContext context)
    { }
  }
}
