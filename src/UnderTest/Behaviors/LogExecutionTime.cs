﻿using System;
using JetBrains.Annotations;

namespace UnderTest.Behaviors
{
  [PublicAPI]
  public class LogExecutionTime: BehaviorAttribute
  {
    private DateTime start;

    public override void OnStepExecuting(StepExecutingContext context)
    {
      base.OnStepExecuting(context);

      start = DateTime.Now;
    }

    public override void OnStepExecuted(StepExecutingContext context)
    {
      base.OnStepExecuted(context);

      context.Handler.Log.Information($"    Step execution time: {Math.Ceiling((DateTime.Now - start).TotalMilliseconds)}ms");
    }
  }
}
