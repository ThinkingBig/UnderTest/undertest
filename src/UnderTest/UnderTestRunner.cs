using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Serilog;
using SimpleInjector.Lifestyles;
using UnderTest.Configuration;
using UnderTest.Exceptions;
using UnderTest.Infrastructure;
using UnderTest.Reporting;

using Console = Colorful.Console;

namespace UnderTest
{
  public class UnderTestRunner: UnderTestRunner<UnderTestCommandLineOptions>
  {}

  [PublicAPI]
  public class UnderTestRunner<TOptionsClass> : IHasLogger
    where TOptionsClass: UnderTestCommandLineOptions, new()
  {
    public UnderTestRunner()
    {
      Console.WriteAscii("UnderTest");
    }

    public ILogger Log => TestSettings.Logger;

    public TestProjectDetails ProjectDetails { get; private set; } = new TestProjectDetails();

    public StrategiesRunner StrategiesRunner { get; private set; } = new StrategiesRunner();

    public TestRunSettings TestSettings { get; private set;} = new TestRunSettings();

    private bool failedToParseArgs { get; set; }

    public UnderTestRunner<TOptionsClass> WithCommandLineArgs(string[] args)
    {
      try
      {
        new UnderTestCommandLineArgsParser(args, TestSettings)
          .ApplyBuiltInArguments(this);
      }
      catch (FailedToParseCommandLineParametersUnderTestException err)
      {
        Log.Fatal($"Failed to parse command line arguments with message: {err.Message}");
        failedToParseArgs = true;
      }

      return this;
    }

    public UnderTestRunner<TOptionsClass> WithProjectDetails(Configure<TestProjectDetails> settings)
    {
      ProjectDetails = settings.InvokeSafe(ProjectDetails);

      return this;
    }

    public UnderTestRunner<TOptionsClass> WithTestSettings(Configure<TestRunSettings> settings)
    {
      TestSettings = settings.InvokeSafe(TestSettings);

      return this;
    }

    public UnderTestRunResult Execute()
    {
      var result = new UnderTestRunResult(ProjectDetails);
      if (failedToParseArgs)
      {
        // todo - add help output of current commandline parameters
        Log.Fatal("Failed to parse command line args.  UnderTest will exit");
        return result;
      }

      using (AsyncScopedLifestyle.BeginScope(TestSettings.Container))
      {
        if (!TestSettings.Init())
        {
          return result;
        }

        try
        {
          TestSettings.TestRunEventHandlers.ForEach(x => x.BeforeTestRun(new BeforeAfterTestRunContext{ }));
          ThrowIfNoFeatureAssemblies();

          var (features, invalidFiles) = LoadAllFeatures();
          features = features
            .FilterBasedOnDebugAttributes(TestSettings)
            .FilterBasedOnExecutionMode(TestSettings);

          Log.LogPreRunInformation(result, TestSettings, features);
          Log.LogInvalidFeatures(invalidFiles);

          ConfigureAndExecuteStrategies(features, result);
        }
        catch (NoFeatureAssembliesConfiguredException)
        {
          Log.OutputNoFeatureAssembliesConfiguredHelp();
        }
        catch (UnderTestFailedToParseFeatureFilesException err)
        {
          Log.OutputInvalidGherkinFailureHelp(err);
        }
        catch (UnderTestFilterFailureException err)
        {
          Log.OutputTestFilterExceptionHelp(err, TestSettings);
        }
        catch (Exception err)
        {
          Log.Error($"General failure during execution: {err.Message}\nwith stacktrace {err.StackTrace}\n" +
                    $"Please verify you are on the latest version of UnderTest projects.");
        }
        finally
        {
          FinishTestRun(result);

          // todo - wrap in try/catch
          TestSettings.TestRunEventHandlers.ForEach(x => x.AfterTestRun(new BeforeAfterTestRunContext{ }));
          result = RunPostRunVerifications(result);

          SummarizeFailures(result);
        }
        
        TestSettings.Dispose();

        return result;
      }
    }

    private void SummarizeFailures(UnderTestRunResult result)
    {
      // todo - refactor me into a new object
      if (result.OverallResult == TestRunOverallResult.Pass)
        return;

      if (!TestSettings.ExecutionSettings.RepeatInconclusiveAndFailuresPostRun)
        return;

      List<ScenarioRunResult> FilterListToOnlyContainScenarioRunResultType(IList<TestRunFeatureResult> featureGroup, ScenarioRunResultType runResultType) =>
        featureGroup
          .SelectMany(group =>
            @group.StrategyRuns.SelectMany(strategy =>
              strategy
                .ScenarioTestResults
                .Where(scenario => scenario.Result == runResultType)))
          .ToList();

      bool HasAnyExpectedExceptions(IList<TestRunFeatureResult> featureGroup) =>
        featureGroup
          .Any(group =>
            @group.StrategyRuns.Any(strategy =>
              strategy
                .ScenarioTestResults
                .Any(scenario => scenario.ExpectedFailure)));

      Log.Information(string.Empty);
      Log.Warning("Repeating inconclusives and failures: \n");

      var index = 1;
      Log.Warning("  Inconclusive:");
      result.Features
        .GroupBy(x => x.FeatureFileName)
        .ForEach(featureGroup=>
        {
          var inconclusiveForFeature = FilterListToOnlyContainScenarioRunResultType(featureGroup.ToList(), ScenarioRunResultType.Inconclusive);
          if (inconclusiveForFeature.Any())
          {
            Log.Warning($"{index++.ToString().PadLeft(5)}) Feature: {featureGroup.First().Name}");
          }

          inconclusiveForFeature.ForEach(scenario => Log.Warning($"      Scenario: {scenario.ScenarioName}"));
        });

      Log.Warning(string.Empty);
      Log.Warning("  Failures:");
      result.Features
        .GroupBy(x => x.FeatureFileName)
        .GroupBy(x => HasAnyExpectedExceptions(x.ToList()))
        .ForEach(expectedGrouping =>
        {
          bool HasAnyFailedScenarios() => FilterListToOnlyContainScenarioRunResultType(expectedGrouping.ToList().SelectMany(x => x.ToList()).ToList(), ScenarioRunResultType.Failed).Any();
          if (HasAnyFailedScenarios())
          {
            Log.Warning(expectedGrouping.Key ? "    Expected:" : "    Unexpected");
          }

          expectedGrouping.ToList().ForEach(featureGroup =>
          {
            var failedScenarios = FilterListToOnlyContainScenarioRunResultType(featureGroup.ToList(), ScenarioRunResultType.Failed);
            if (failedScenarios.Any())
            {
              Log.Warning($"{index++.ToString().PadLeft(5)}) Feature: {featureGroup.First().Name}");
              failedScenarios
                .Where(x => !string.IsNullOrEmpty(x.ScenarioName))
                .ForEach(grouping => Log.Warning($"      Scenario: {grouping.ScenarioName}"));
            }  
          });
        });
    }

    private UnderTestRunResult RunPostRunVerifications(UnderTestRunResult result) =>
      TestSettings.PostRunVerification
        .Aggregate(result, (current, runVerification) => runVerification(current));

    private void FinishTestRun(UnderTestRunResult result)
    {
      result.FinishRun();
      Log.LogTestResults(result, TestSettings);
      TestSettings.ReportOutputWriter.SaveResults(result);
    }

    private void ConfigureAndExecuteStrategies(FeaturesContainer features, UnderTestRunResult result)
    {
      StrategiesRunner.TestSettings = TestSettings;
      StrategiesRunner.Strategies = TestSettings.Strategies;
      StrategiesRunner.FeatureRunner = new FeatureRunner {TestSettings = TestSettings};
      StrategiesRunner.ExecuteFeaturesForAllStrategies(features, result);
    }

    private (FeaturesContainer, IList<UnderTestFailedToParseFeatureFilesException>) LoadAllFeatures()
    {
      var loader = TestSettings.GherkinLoader;
      var (features, invalid) = loader
        .LoadAllFromFolder(TestSettings.FeatureDirectory);

      return (features.BuildFeaturesContainer(), invalid);
    }

    private void ThrowIfNoFeatureAssemblies()
    {
      if (!TestSettings.Assemblies.Any())
      {
        throw new NoFeatureAssembliesConfiguredException();
      }
    }
  }
}
