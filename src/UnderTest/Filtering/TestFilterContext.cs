using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;
using UnderTest.Strategies;
using Examples = Gherkin.Events.Args.Ast.Examples;

namespace UnderTest.Filtering
{
  [PublicAPI]
  public class TestFilterContext
  {
    public FeatureContext FeatureContext { get; set; }

    [CanBeNull]
    public Scenario Scenario { get; set; }
    
    public Gherkin.Ast.Examples SpecificExample { get; set; } 

    public ITestStrategy TestStrategy { get; set; }

    public IEnumerable<Tag> Tags => BuildTagListFromFeature();

    public IEnumerable<Tag> AllFeatureTags => BuildCompleteTagListFromFeature();
    public IEnumerable<Tag> FeatureTags => FeatureContext.Document.Feature.Tags;

    private IEnumerable<Tag> BuildCompleteTagListFromFeature()
    {
      var tags = BuildTagListFromFeature().ToList();

      // now add the tags for all of our children
      var scenarioTags = GatherAllScenarios(FeatureContext.Document.Feature.Children)
        .SelectMany(x =>
        {
          var result = new List<Tag>();
          result.AddRange(x.Tags);
          x.Examples.ToList().ForEach(example =>
          {
            if (example.Tags.Any())
            {
              result.AddRange(example.Tags);
            }
          });
          return result;
        });

      tags.AddRange(scenarioTags);

      return tags.Distinct();
    }

    private IEnumerable<Tag> BuildTagListFromFeature()
    {
      var tags = new List<Tag>();
      if (FeatureContext?.Document?.Feature.Tags != null)
      {
        tags.AddRange(FeatureContext.Document.Feature.Tags);
      }

      if (Scenario != null)
      {
        if (Scenario.Tags != null)
        {
          tags.AddRange(Scenario.Tags);
        }
        
        Scenario.Examples?.ForEach(example =>
        {
            if (example.Tags.Any())
            {
              tags.AddRange(example.Tags);
            }
        });
      }

      return tags;
    }

    private static IList<Scenario> GatherAllScenarios(IEnumerable<IHasLocation> parents)
    {
      var result = new List<Scenario>();

      foreach (var child in parents)
      {
        switch (child)
        {
          case Background _:
            break;
          case Scenario scenario:
            result.Add(scenario);
            break;
          case Rule rule:
            var children = GatherAllScenarios(rule.Children);
            if (children.Any())
            {
              result.AddRange(children);
            }
            break;
        }
      }

      return result;
    }
  }
}
