namespace UnderTest.Attributes.StepParams
{
  public class YesNoStepParamAttribute : StepParamAttribute
  {
    public override object Transform(object input)
    {
      return "yes".Equals(input?.ToString().ToLowerInvariant());
    }
  }
}
