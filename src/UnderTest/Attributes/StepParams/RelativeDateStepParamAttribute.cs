using System;
using JetBrains.Annotations;
using UnderTest.Exceptions;

namespace UnderTest.Attributes.StepParams
{
  [PublicAPI]
  public class RelativeDateStepParamAttribute : StepParamAttribute
  {
    public override object Transform(object input)
    {
      if (input == null)
      {
        return null;
      }

      var value = input.ToString();
      switch (value.ToLowerInvariant())
      {
        case "today":
          return DateTime.UtcNow.Date;
        case "tomorrow":
          return DateTime.UtcNow.Date.AddDays(1);
        case "yesterday":
          return DateTime.UtcNow.Date.Subtract(TimeSpan.FromDays(1));
        default:
          throw new UnsupportedRelativeTimeStepParamValueException($"Unsupported relative time value {value}");
      }
    }
  }
}
