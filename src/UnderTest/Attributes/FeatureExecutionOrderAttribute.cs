using System;
using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [PublicAPI]
  [AttributeUsage(AttributeTargets.Class)]
  public class FeatureExecutionOrderAttribute : Attribute
  {
    public int Order { get; set; }
  }
}