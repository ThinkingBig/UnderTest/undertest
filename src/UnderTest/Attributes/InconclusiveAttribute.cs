using System;
using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [MeansImplicitUse]
  [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = true)]
  public class InconclusiveAttribute : Attribute
  {
    public string Message { get; set; }
  }
}
