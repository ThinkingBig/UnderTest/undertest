﻿using System;
using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [MeansImplicitUse]
  public class ScenarioAttribute : Attribute
  {
    public ScenarioAttribute(string name) => Name = name;
    
    public string Name { get; }
  }
}
