using System;
using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [MeansImplicitUse]
  [BaseTypeRequiredAttribute(typeof(FeatureHandler))]
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
  public class HandlesFeatureAttribute : Attribute
  {
    public HandlesFeatureAttribute(string featureFileName)
    {
      FeatureFileName = featureFileName ?? throw new ArgumentNullException(nameof(featureFileName));
    }

    public string FeatureFileName { get; }
  }
}
