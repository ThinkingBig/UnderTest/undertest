﻿using System;
using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [MeansImplicitUse]
  [BaseTypeRequired(typeof(TestRunEventHandlerBase))]
  public class TestRunEventHandlerAttribute: Attribute
  { }
}
