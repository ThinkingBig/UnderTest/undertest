using System;
using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [MeansImplicitUse]
  [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
  public class PropertyInjectedAttribute : Attribute
  { }
}
