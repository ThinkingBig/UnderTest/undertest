using System;
using JetBrains.Annotations;

namespace UnderTest.Attributes
{
  [MeansImplicitUse]
  public class GivenAttribute : StepAttribute
  {
    public GivenAttribute(string step) : base(step)
    { }
  }
}
