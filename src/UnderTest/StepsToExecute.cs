using System.Threading.Tasks;

namespace UnderTest
{
  public delegate Task StepsToExecute<in T>(T settings);
  public delegate void StepsToExecuteSync<in T>(T settings);
}
