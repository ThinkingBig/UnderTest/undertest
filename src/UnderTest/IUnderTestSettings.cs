﻿using System.Collections.ObjectModel;
using System.Reflection;
using Serilog;
using SimpleInjector;
using UnderTest.Filtering;
using UnderTest.Infrastructure;
using UnderTest.Locators;

namespace UnderTest
{
  public interface IUnderTestSettings
  {
    ReadOnlyCollection<Assembly> Assemblies { get; }

    string AssetsFolder { get; }

    string[] CommandLineArgs { get; }

    UnderTestConfigSettings ConfigSettings { get; }

    Container Container { get; }

    IFeatureHandlerLocator FeatureHandlerLocator { get; }

    IGherkinLoader GherkinLoader { get; }

    ILogger Logger { get; }

    TestRunOutputSettings OutputSettings { get; }

    ReadOnlyCollection<TestFilter> TestFilters { get; }

    string FeatureDirectory { get;  }

    TestRunExecutionSettings ExecutionSettings { get; }

    ReadOnlyCollection<TestRunEventHandlerBase> TestRunEventHandlers { get; }
  }
}
