using System.Collections.Generic;
using System.IO;

namespace UnderTest.Infrastructure.Preflight
{
  public class CheckForPotentiallyInvalidHandlersFeatureFilePaths : IPreflightCheck
  {
    public void Execute(TestRunSettings settings)
    {
      settings.TypesCache.FeatureFilenames.ForEach(x =>
      {
        if (!File.Exists(x))
        {
          settings.Logger.Error($"Found `HandlesFeatureAttribute` with filepath `{x}` but the file does not exist.");
        }
      });
    }
  }
}
