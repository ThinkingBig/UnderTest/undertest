using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Serilog;
using UnderTest.Filtering;
using UnderTest.Reporting;
using UnderTest.Strategies;

namespace UnderTest.Infrastructure
{
  public class StrategiesRunner : IHasLogger, IHasInternalTestSettings
  {
    public ILogger Log => TestSettings.Logger;

    public TestRunSettings TestSettings { get; set; }

    public FeatureRunner FeatureRunner { get; set; }

    public ReadOnlyCollection<ITestStrategy> Strategies { get; set; }

    public void ExecuteFeaturesForAllStrategies(FeaturesContainer features, UnderTestRunResult result)
    {
      foreach (var strategy in Strategies)
      {
        if (!ProcessFeaturesWithStrategy(strategy, features, result))
        {
          return;
        }
      }
    }

    private bool ProcessFeaturesWithStrategy(ITestStrategy strategy, FeaturesContainer features, UnderTestRunResult result)
    {
      AddWipFeaturesToTestResults(strategy, features.Wip, result);
      AddIgnoredFeaturesToTestResults(strategy, features.Ignored, result);

      var featuresToRun = RemoveFilteredFeaturesAndAddToTestResults(strategy, features.Testable, result);

      return ExecuteNonIgnoredFeatures(strategy, featuresToRun, result);
    }

    private void AddWipFeaturesToTestResults(ITestStrategy strategy, FeatureContextList features, UnderTestRunResult result)
    {
      // add the ignored to our test results
      features.ForEach(x =>
      {
        Log.Information($"Skipping wip feature: {x.Filename}");
        result
          .Features
          .EnsureFeatureInTestResultAndReturn(x)
          .StrategyRuns
          .Add(new TestRunFeatureStrategyResult
          {
            Duration = TimeSpan.Zero,
            StartTime = DateTime.UtcNow,
            EndTime = DateTime.UtcNow,
            StrategyName = strategy.GetType().Name,
            ScenarioTestResults = x.Document.Feature.Children.ToWipTestResults()
          });
      });
    }

    private void AddIgnoredFeaturesToTestResults(ITestStrategy testStrategy, FeatureContextList features, UnderTestRunResult testResult)
    {
      features.ForEach(x =>
      {
        Log.Information($"Skipping ignored feature: {x.Filename}");
        testResult
          .Features
          .EnsureFeatureInTestResultAndReturn(x)
          .StrategyRuns
          .Add(new TestRunFeatureStrategyResult
          {
            Duration = TimeSpan.Zero,
            StartTime = DateTime.UtcNow,
            EndTime = DateTime.UtcNow,
            StrategyName = testStrategy.GetType().Name,
            ScenarioTestResults = x.Document.Feature.Children.ToIgnoredTestResults()
          });
      });
    }

    private FeatureContextList RemoveFilteredFeaturesAndAddToTestResults(ITestStrategy strategyP, FeatureContextList featuresP,
      UnderTestRunResult resultP)
    {
      var features = new FeatureContextList(TestSettings);

      featuresP.ForEach(featureContext =>
      {
        var filterContext = new TestFilterContext
        {
          FeatureContext = featureContext,
          TestStrategy = strategyP
        };

        if (featureContext.ShouldBeFiltered(TestSettings.TestFilters, filterContext))
        {
          Log.Information($"Filtering feature: {featureContext.Filename} No scenarios will be run.");

          resultP
            .Features
            .EnsureFeatureInTestResultAndReturn(featureContext)
            .StrategyRuns
            .Add(new TestRunFeatureStrategyResult
            {
              Duration = TimeSpan.Zero,
              StartTime = DateTime.UtcNow,
              EndTime = DateTime.UtcNow,
              StrategyName = strategyP.GetType().Name,
              ScenarioTestResults = featureContext.Document.Feature.Children.ToIgnoredTestResults()
            });
          return;
        }

        features.Add(featureContext);
      });

      return features;
    }

    private bool ExecuteNonIgnoredFeatures(ITestStrategy testStrategy, FeatureContextList features, UnderTestRunResult testResult)
    {
      features = ApplyPriorityToFeatureOrder(features);

      var hasHadFailure = false;
      features
        .ForEach(featureContext =>
        {
          var strategyTestRunResult = new TestRunFeatureStrategyResult
          {
            StrategyName = testStrategy.GetType().Name,
            StartTime = DateTime.UtcNow
          };

          try
          {
            if (hasHadFailure && TestSettings.ExecutionSettings.FailRunOnFirstError)
            {
              Log.Warning($"'{featureContext.Filename}' being skipped as a previous scenario failed.  This scenario will be skipped.");
              strategyTestRunResult.SetAndNoOpWithStrategyName(testStrategy.Name);
              strategyTestRunResult.AddFeatureScenariosAsSkipped(featureContext);

              return;
            }

            Log.Information(string.Empty);
            Log.PrintHeader(featureContext.Filename);

            FeatureRunner.Execute(featureContext, strategyTestRunResult, testStrategy);
          }
          finally
          {
            strategyTestRunResult.EndTime = DateTime.UtcNow;
            strategyTestRunResult.Duration = strategyTestRunResult.EndTime - strategyTestRunResult.StartTime;
            testResult
              .Features
              .EnsureFeatureInTestResultAndReturn(featureContext)
              .StrategyRuns
              .Add(strategyTestRunResult);

            hasHadFailure |= testResult.HasAnyFailedScenarios();
          }
        });

      return hasHadFailure;
    }

    private FeatureContextList ApplyPriorityToFeatureOrder(FeatureContextList features)
    {
      return new FeatureContextList(features.OrderByDescending(featureContext => featureContext.ExecutionOrder).ToList(), TestSettings);
    }
  }
}
