using System.Collections.Generic;
using System.IO;
using GlobExpressions;
using Serilog;
using UnderTest.Exceptions;

namespace UnderTest.Infrastructure
{
  internal class FileSystemGherkinLoader : IGherkinLoader
  {
    public FileSystemGherkinLoader(TestRunSettings settings)
    {
      TestSettings = settings;
    }

    public ILogger Log => TestSettings.Logger;

    public IUnderTestSettings TestSettings { get; }

    public (FeatureContextList, IList<UnderTestFailedToParseFeatureFilesException>) LoadAllFromFolder(string targetFolder)
    {
      var files = Glob.Files(targetFolder, @"**\*.feature",
        GlobOptions.CaseInsensitive);
      var list = new List<FeatureContext>();
      var invalid = new List<UnderTestFailedToParseFeatureFilesException>();
      foreach (var file in files)
      {
        try
        {
          list.Add(new FeatureContext
          {
            Document = new Gherkin.Parser().Parse(Path.Combine(targetFolder, file)),
            Filename = file,
            TestSettings = TestSettings
          });
        }
        catch (Gherkin.CompositeParserException err)
        {
          if (TestSettings.ExecutionSettings.FailRunOnFirstError)
          {
            throw new UnderTestFailedToParseFeatureFilesException("Failed to parse gherkin.")
            {
              Filename = file,
              ParseException = err
            };
          }

          invalid.Add(new UnderTestFailedToParseFeatureFilesException("Failed to parse gherkin.")
          {
            Filename = file,
            ParseException = err
          });
        }
      }

      return (new FeatureContextList(list, TestSettings), invalid);
    }
  }
}
