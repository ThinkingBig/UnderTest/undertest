using System;
using JetBrains.Annotations;

namespace UnderTest.Infrastructure
{
  [PublicAPI]
  public class TestRunExecutionSettings
  {
    public bool AllowInScenarioAndExternalExamples { get; set; } = true;

    public TimeSpan ScenarioTimeout { get; set; } = TimeSpan.Zero;

    public bool FailRunOnFirstError { get; set; } = false;

    public bool RepeatInconclusiveAndFailuresPostRun { get; set; } = true;

    public TestRunExecutionSettings SetAllowInScenarioAndExternalExamples(bool value)
    {
      AllowInScenarioAndExternalExamples = value;

      return this;
    }

    public TestRunExecutionSettings SetFailRunOnFirstError(bool value)
    {
      FailRunOnFirstError = value;

      return this;
    }

    public TestRunExecutionSettings SetRepeatInconclusiveAndFailuresPostRun(bool value)
    {
      RepeatInconclusiveAndFailuresPostRun = value;

      return this;
    }

    public TestRunExecutionSettings SetScenarioTimeout(TimeSpan timeout)
    {
      ScenarioTimeout = timeout;

      return this;
    }
  }
}
