using System.Diagnostics;
using System.Reflection;
using JetBrains.Annotations;

namespace UnderTest.Infrastructure
{
  [PublicAPI]
  public class TestProjectDetails
  {
    public string ProjectName { get; private set; }

    public string ProjectVersion { get; private set; }

    public string ProjectDescription { get; set; }

    public TestProjectDetails SetProjectName(string projectName)
    {
      ProjectName = projectName;

      return this;
    }

    public TestProjectDetails SetProjectVersion(string version)
    {
      ProjectVersion = version;

      return this;
    }

    public TestProjectDetails SetProjectVersionFromAssembly(Assembly assembly)
    {
      ProjectVersion = FileVersionInfo.GetVersionInfo(assembly.Location).ProductVersion;

      if (string.IsNullOrWhiteSpace(ProjectVersion))
      {
        ProjectVersion = assembly.GetName().Version.ToString();
      }

      return this;
    }

    public TestProjectDetails SetProjectDescription(string description)
    {
      ProjectDescription = description;

      return this;
    }
  }
}
