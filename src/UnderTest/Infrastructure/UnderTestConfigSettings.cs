namespace UnderTest.Infrastructure
{
  public class UnderTestConfigSettings
  {
    public string FutureVersionTagName { get; internal set; } = "@future-version";

    public string InconclusiveTagName { get; internal set; } = "@inconclusive";

    public string IgnoreTagName { get; internal set; } = "@ignore";

    public string WipTagName { get; internal set; } = "@wip";
  }
}
