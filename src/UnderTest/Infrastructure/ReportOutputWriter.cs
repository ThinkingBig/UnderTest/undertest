using System.IO;
using Newtonsoft.Json;
using Serilog;
using UnderTest.Reporting;

namespace UnderTest.Infrastructure
{
  public class ReportOutputWriter : IReportOutputWriter, IHasTestSettings, IHasLogger
  {
    public ReportOutputWriter(IUnderTestSettings runSettings)
    {
      TestSettings = runSettings;
    }

    public ILogger Log => TestSettings.Logger;

    public IUnderTestSettings TestSettings { get; }

    public void SaveResults(UnderTestRunResult results)
    {
      // we set TypeNameHandling to All so we can map behavior types back in successfully.
      // this means when we read these types back in anonymously, we need to verify the types using a https://www.newtonsoft.com/json/help/html/SerializeSerializationBinder.htm
      var jsonSettings = new JsonSerializerSettings
      {
        TypeNameHandling = TypeNameHandling.All
      };

      File.WriteAllText(TestSettings.OutputSettings.ReportOutputFilePath, JsonConvert.SerializeObject(results, jsonSettings));

      CopyAssetsToReportFolder();
    }

    private void CopyAssetsToReportFolder()
    {
      var sourceAssetsPath = TestSettings.AssetsFolder;

      if (!Directory.Exists(sourceAssetsPath))
      {
        Log.Warning("Assets folder does not exist.  Not copying assets to report folder");
        return;
      }

      var targetFolder = Path.Combine(Directory.GetCurrentDirectory(), TestSettings.OutputSettings.ReportOutputFolder, "assets");

      if (!Directory.Exists(targetFolder))
      {
        Directory.CreateDirectory(targetFolder);
      }

      CopyFilesRecursively(
        new DirectoryInfo(sourceAssetsPath),
        new DirectoryInfo(targetFolder));
    }

    private static void CopyFilesRecursively(DirectoryInfo source, DirectoryInfo target)
    {
      foreach (var dir in source.GetDirectories())
      {
        CopyFilesRecursively(dir, target.CreateSubdirectory(dir.Name));
      }

      foreach (var file in source.GetFiles())
      {
        file.CopyTo(Path.Combine(target.FullName, file.Name), true);
      }
    }
  }
}
