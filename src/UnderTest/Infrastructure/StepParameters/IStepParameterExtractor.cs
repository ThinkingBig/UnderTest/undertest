namespace UnderTest.Infrastructure.StepParameters
{
  public interface IStepParameterExtractor: IHasTestSettings
  {
    StepParameterList Extract(string input);
  }
}
