using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using UnderTest.Attributes;
using UnderTest.Exceptions;

namespace UnderTest.Infrastructure
{
  public class TypesCache
  {
    public IDictionary<string, Type> FeatureHandlers { get; } = new Dictionary<string, Type>();

    public List<string> FeatureFilenames = new List<string>();

    public IList<Type> GlobalHandlerTypes { get; private set; }

    public void BuildTypeCache(TestRunSettings testRunSettings)
    {
      BuildStepAttributeList(testRunSettings);

      CreateFeatureHandlers(testRunSettings);
    }

    public object LocateInstanceForType(Type targetTypeP, TestRunSettings settings)
    {
      var cachedObject = FeatureHandlers
                          .Values
                          .FirstOrDefault(x => x.GetType() == targetTypeP);
      if (cachedObject != null)
      {
        settings.Logger.Verbose($"  Using a cached version of {targetTypeP.Name}");
        return cachedObject;
      }

      settings.Logger.Verbose($"  Creating a new instance of the handler of type {targetTypeP.Name}");
      var instance = settings.Container.GetInstance(targetTypeP);
      switch (instance)
      {
        case FeatureHandler handler:
          handler.TestSettings = settings;
          break;
        case IGlobalHandler globalHandler:
          globalHandler.TestSettings = settings;
          break;
      }

      return instance;
    }

    private void BuildStepAttributeList(TestRunSettings testRunSettings)
    {
      GlobalHandlerTypes =
        testRunSettings
          .Assemblies
          .SelectMany(assembly => assembly
            .GetTypes()
            .Where(type => typeof(IGlobalHandler).IsAssignableFrom(type))
            .ToList())
          .ToList();
    }

    private void CreateFeatureHandlers(TestRunSettings testRunSettings)
    {
      FeatureHandlers.Clear();

      testRunSettings
        .Assemblies
        .SelectMany(x => x.GetTypes().Where(type => typeof(FeatureHandler).IsAssignableFrom(type)))
        .ForEach(type =>
        {
          if (type.IsAbstract)
          {
            testRunSettings.Logger.Information($"Skipping abstract FeatureHandler {type.Name}");
            return;
          }

          if (type.IsGenericTypeDefinition)
          {
            return;
          }

          type
            .GetCustomAttributes(typeof(HandlesFeatureAttribute), true)
            .Cast<HandlesFeatureAttribute>()
            .ForEach(attr =>
            {
              FeatureFilenames.Add(Path.Combine(testRunSettings.FeatureDirectory, attr.FeatureFileName));
              var key = attr.FeatureFileName.NormalizeFilePath();
              if (FeatureHandlers.ContainsKey(key))
              {
                throw new DuplicateFeatureFileNameException($@"There are currently more than one HandlesFeature attributes which are associated with the feature file '{attr.FeatureFileName}'. HandlesFeature to Feature is a one-to-one relationship. UnderTest will exit.")
                {
                  FeatureFileName = attr.FeatureFileName
                };
              }

              FeatureHandlers.Add(key, type);
            });
        });
    }
  }
}
