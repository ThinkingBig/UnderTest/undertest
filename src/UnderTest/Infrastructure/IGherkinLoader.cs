using System.Collections.Generic;
using UnderTest.Exceptions;

namespace UnderTest.Infrastructure
{
  public interface IGherkinLoader : IHasLogger, IHasTestSettings
  {
    (FeatureContextList, IList<UnderTestFailedToParseFeatureFilesException>) LoadAllFromFolder(string targetFolder);
  }
}
