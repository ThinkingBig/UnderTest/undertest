namespace UnderTest.Infrastructure
{
  public enum GherkinStepKeywordType
  {
    Given = 1,

    When = 2,

    Then = 3
  }
}
