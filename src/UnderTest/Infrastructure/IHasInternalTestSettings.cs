﻿namespace UnderTest.Infrastructure
{
  public interface IHasInternalTestSettings
  {
    TestRunSettings TestSettings { get; }
  }
}
