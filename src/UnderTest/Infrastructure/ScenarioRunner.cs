using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Gherkin.Ast;
using Serilog;
using UnderTest.Exceptions;
using UnderTest.Reporting;
using UnderTest.Strategies;

namespace UnderTest.Infrastructure
{
  internal class ScenarioRunner
  {
    public IFeatureHandler Handler { get; set; }

    public TestRunSettings TestSettings { get; set; }

    public List<StepForProcessing> BackgroundSteps { get; set; }

    public Scenario Scenario { get; set; }

    public ExampleTableRow ExampleTableRow { get; set; }

    public FeatureContext FeatureContext { get; set; }

    public TestRunFeatureStrategyResult StrategyTestRunResult { get; set; }

    public ITestStrategy TestStrategy { get; set; }

    private ILogger Log => TestSettings.Logger;

    private bool passedSoFar;

    private bool inconclusive;

    public void Execute()
    {
      Log.Information($"Executing scenario: ==> {Scenario.Name}");

      inconclusive = Scenario.Tags.IncludesInconclusiveTag(TestSettings.ConfigSettings);
      passedSoFar = !inconclusive;

      var scenarioContext = new BeforeAfterScenarioContext(FeatureContext, Scenario);
      var scenarioRunResult = new ScenarioRunResult
      {
        Keyword = Scenario.Keyword,
        Description = Scenario.Description,
        ScenarioName = Scenario.Name,
        ExpectedFailure = Handler.HasExpectedFailureAttribute()
      };

      try
      {
        if (CallAndFailScenarioOnException(
          () => scenarioContext.TriggerBeforeScenarioEvent(TestStrategy, Handler, TestSettings),
          "BeforeScenario",
          scenarioRunResult))
        {
          ProcessScenarioSteps(BackgroundSteps, Scenario, scenarioRunResult, Handler, ExampleTableRow);
        }
      }
      finally
      {
        CallAndFailScenarioOnException(
          () => scenarioContext.TriggerAfterScenarioEvent(TestStrategy, Handler, TestSettings),
          "AfterScenario",
          scenarioRunResult);

        StrategyTestRunResult.ScenarioTestResults.Add(scenarioRunResult);
      }
    }

    private void ProcessScenarioSteps(List<StepForProcessing> backgroundSteps, Scenario scenario, ScenarioRunResult scenarioRunResult,
      IFeatureHandler handler, ExampleTableRow exampleTableRow)
    {
      void ExecuteScenarioSteps()
      {
        var stepsToExecute = GroupStepsByKeyword(scenario.Steps.ToStepsForProcessingList());
        stepsToExecute.ForEach(group =>
        {
          var groupContext = new BeforeAfterKeywordGroupingContext
          {
            Feature = FeatureContext,
            Scenario = scenario,
            ScenarioRunResult = scenarioRunResult,
            KeywordGrouping = @group
          };

          try
          {
            groupContext.TriggerBeforeEvent(TestStrategy, handler, TestSettings);
            @group.Steps.ForEach(step =>
            {
              step.ExampleTableRow = exampleTableRow;
              ExecuteStep(scenario, step, scenarioRunResult, handler);
            });
          }
          finally
          {
            groupContext.TriggerAfterEvent(TestStrategy, handler, TestSettings);
          }
        });
      }

      void ExecuteBackgroundSteps()
      {
        var stepsToExecute = GroupStepsByKeyword(backgroundSteps);
        stepsToExecute.ForEach(group =>
        {
          var groupContext = new BeforeAfterKeywordGroupingContext
          {
            Feature = FeatureContext,
            Scenario = scenario,
            ScenarioRunResult = scenarioRunResult,
            KeywordGrouping = @group
          };

          try
          {
            groupContext.TriggerBackgroundBeforeEvent(TestStrategy, handler, TestSettings);
            @group.Steps.ForEach(step =>
            {
              step.ExampleTableRow = exampleTableRow;
              ExecuteStep(scenario, step, scenarioRunResult, handler);
            });
          }
          finally
          {
            groupContext.TriggerBackgroundAfterEvent(TestStrategy, handler, TestSettings);
          }
        });
      }

      void ExecuteScenarioMethod(MethodInfo scenarioMethod)
      {
        if (TestSettings.OutputSettings.WarnAboutScenarioExecution)
        {
          Log.Warning($"    Scenario execution removes the tie between scenario steps and their behavior.  Proceed with care.");  
        }
        
        var stopWatch = Stopwatch.StartNew();
        var steps = scenario.Steps.ToStepsForProcessingList();
        var firstStep = steps.FirstOrDefault();
        try
        {
          if (inconclusive || !passedSoFar)
          {
            HandleStepAsSkipped(scenarioRunResult, firstStep);
            return;
          }
          
          // if we are in OnlyExecuteGivens mode - skip this 
          if (TestSettings.ExecutionMode == ExecutionMode.OnlyExecuteGivens)
          {
            HandleStepAsSkippedBecauseOfExecutionMode(scenarioRunResult, firstStep);
            return;
          }

          Log.Information($"  Executing scenario method: '{scenario.Name} {(exampleTableRow != null ? $"`{exampleTableRow}`" : string.Empty )}");
          
          scenarioMethod?.ThrowIfInconclusive();
          var containerInstance = TestSettings.TypesCache.LocateInstanceForType(scenarioMethod?.ReflectedType, TestSettings);
          var parameters = new List<object>();
          if (scenarioMethod != null && scenarioMethod.GetParameters().Any())
          {
            parameters.Add(new ScenarioExecutionContext(FeatureContext, scenario, exampleTableRow));
          }
            
          scenarioMethod
            ?.Invoke(containerInstance, parameters.ToArray())
            .WaitIfTask();
          
          // all pass
          steps.ForEach(step =>
          {
            scenarioRunResult.StepResults.Add(new ScenarioStepResult
            {
              HandlerName = handler?.GetType().Name,
            }.LoadStep(step));
          });
        }
        catch (MethodMarkedAsInconclusiveException e)
        {
          Log.Warning($"  Method '{e.Method.Name}' is marked inconclusive. {e.Message}.  Scenario will be skipped.");
          steps.ForEach(step => HandleStepAsSkipped(scenarioRunResult, step));
          inconclusive = true;
        }
        catch (Exception e)
        {
          switch (e.InnerException)
          {
            case WorkInProgressScenarioException wip:
              Log.Warning($"  Scenario '{wip.StepKeyword} {wip.StepText}' is marked wip.  Scenario will be skipped.");
              steps.ForEach(step => HandleStepAsSkipped(scenarioRunResult, step));
              inconclusive = true;
              break;
            case ScenarioExecutionTimeoutException timeoutException:
              Log.Error($"  Scenario '{scenario.Keyword} {scenario.Name}' timed out.  Scenario will be considered failed.");
              steps.ForEach(step => HandleStepAsException(scenarioRunResult, timeoutException, stopWatch, step));
              passedSoFar = false;
              break;
            default:
            {
              if (scenarioRunResult.ExpectedFailure)
              {
                Log.Warning("  This is an expected failure.  The Feature or Step are marked as expected failures.");  
              }
            
              if (e is StepCallWithParameterCountMismatchException err)
              {
                var message = $"\n    Failure reason: The number of parameters({err.IntendedParameterCount}) passed to the step handler method `{handler?.GetType().Name}.{err.MethodName}` did not match the number of parameters expected.\n" +
                              $"    This is generally caused by an invalid step selector or an unintended step method parameter.\n" +
                              $"      Step selector: {firstStep.Text}\n" +
                              $"      Method signature: {err.MethodCallSignature}\n";
                Log.Error(message);
              }
              
              steps.ForEach(step => HandleStepAsException(scenarioRunResult, e, stopWatch, step));

              break;
            }
          }
        }

        ProcessScenarioRunResult(scenarioRunResult);
      }
      
      ExecuteBackgroundSteps();

      var (hasScenarioMethod, method) = handler.HasScenarioAttribute(scenario);
      if (hasScenarioMethod)
      {
        ExecuteScenarioMethod(method);
      }
      else
      {
        ExecuteScenarioSteps();  
      }
      
      LogResult();
    }

    private IList<ScenarioKeywordGrouping> GroupStepsByKeyword(IEnumerable<StepForProcessing> backgroundSteps)
    {
      var result = new List<ScenarioKeywordGrouping>();

      var keyword = string.Empty;
      var next = new ScenarioKeywordGrouping();
      foreach (var step in backgroundSteps)
      {
        if (NonContinuationKeyword(step, keyword))
        {
          next = new ScenarioKeywordGrouping
          {
            Keyword = (GherkinStepKeywordType)Enum.Parse(typeof(GherkinStepKeywordType), step.Keyword)
          };
          result.Add(next);
        }

        next.Steps.Add(step);
        keyword = step.Keyword;
      }

      return result;
    }

    private bool NonContinuationKeyword(StepForProcessing currentStep, string currentKeyword)
    {
      return !"and".Equals(currentStep.Keyword.Trim(), StringComparison.CurrentCultureIgnoreCase)
             &&
             !"but".Equals(currentStep.Keyword.Trim(), StringComparison.CurrentCultureIgnoreCase)
             &&
             !currentKeyword.Equals(currentStep.Keyword.Trim(), StringComparison.CurrentCultureIgnoreCase);
    }

    private void ExecuteStep(Scenario scenario, StepForProcessing step, ScenarioRunResult scenarioRunResult,
      IFeatureHandler handler)
    {
      var stopWatch = Stopwatch.StartNew();

      try
      {
        if (inconclusive || !passedSoFar)
        {
          HandleStepAsSkipped(scenarioRunResult, step);
          return;
        }

        // if we are in OnlyExecuteGivens mode - ignore any non-given steps
        if (TestSettings.ExecutionMode == ExecutionMode.OnlyExecuteGivens
          && step.Keyword.Trim().ToLowerInvariant() != "given")
        {
          HandleStepAsSkippedBecauseOfExecutionMode(scenarioRunResult, step);
          return;
        }

        var stepResult = new ScenarioStepResult
        {
          HandlerName = handler?.GetType().Name,
        }.LoadStep(step);

        var stepContext = new BeforeAfterStepContext
        {
          Feature = FeatureContext,
          Scenario = scenario,
          Step = step,
          ScenarioStepResult = stepResult
        };

        try
        {
          Log.Information(
            $"  Executing step '{step.Keyword}{step.ExampleTableRow?.ApplyRowToStepText(step.Text) ?? step.Text}'");
          stepContext.TriggerBeforeStepEvent(TestStrategy, handler, TestSettings);
          step.Execute(TestSettings, stepResult, handler, stepContext, scenarioRunResult);
          scenarioRunResult.StepResults.Add(stepResult);
        }
        catch (MethodMarkedAsInconclusiveException e)
        {
          Log.Warning($"  Method '{e.Method.Name}' is marked inconclusive. {e.Message}.  Scenario will be skipped.");
          HandleStepAsSkipped(scenarioRunResult, step);
          inconclusive = true;
        }
        finally
        {
          stepContext.TriggerAfterStepEvent(TestStrategy, handler, TestSettings);
        }
      }
      catch (MissingStepBindingException e)
      {
        HandleStepAsMissingStepBinding(scenarioRunResult, e, stopWatch, step);
      }
      catch (Exception e)
      {
        switch (e.InnerException)
        {
          case WorkInProgressScenarioException wip:
            Log.Warning($"  Step '{wip.StepKeyword} {wip.StepText}' is marked wip.  Scenario will be skipped.");
            HandleStepAsSkipped(scenarioRunResult, step);
            inconclusive = true;
            break;
          case ScenarioExecutionTimeoutException timeoutException:
            Log.Error($"  Step '{step.Keyword} {step.Text}' timed out.  Scenario will be considered failed.");
            HandleStepAsException(scenarioRunResult, timeoutException, stopWatch, step);
            passedSoFar = false;
            break;
          default:
          {
            if (scenarioRunResult.ExpectedFailure)
            {
              Log.Warning("  This is an expected failure.  The Feature or Step are marked as expected failures.");  
            }
            
            if (e is StepCallWithParameterCountMismatchException err)
            {
              var message = $"\n    Failure reason: The number of parameters({err.IntendedParameterCount}) passed to the step handler method `{handler?.GetType().Name}.{err.MethodName}` did not match the number of parameters expected.\n" +
                            $"    This is generally caused by an invalid step selector or an unintended step method parameter.\n" +
                            $"      Step selector: {step.Text}\n" +
                            $"      Method signature: {err.MethodCallSignature}\n";
              Log.Error(message);
            }

            HandleStepAsException(scenarioRunResult, e, stopWatch, step);

            break;
          }
        }
      }

      ProcessScenarioRunResult(scenarioRunResult);
    }

    private void HandleStepAsException(ScenarioRunResult scenarioRunResult, Exception e, Stopwatch stopWatch,
      StepForProcessing step)
    {
      e.OutputException(TestSettings, "Failed to process feature with exception:\n  ");

      passedSoFar = false;

      scenarioRunResult.StepResults.Add(new ScenarioStepResult
      {
        Duration = stopWatch.Elapsed,
        ScenarioStepResultType = ScenarioStepResultType.Failed,
        LogMessages =
        {
          new TestResultLogMessage
          {
            Type = "ERROR",
            Message = (e.InnerException ?? e).Message
          }
        }
      }.LoadStep(step));
    }

    private void HandleStepAsMissingStepBinding(ScenarioRunResult scenarioRunResult, MissingStepBindingException e,
      Stopwatch stopWatch, StepForProcessing step)
    {
      Log.Error($"Missing step binding '{e.StepKeyword}{e.StepText}' - this feature will be marked as inconclusive.");
      inconclusive = true;

      scenarioRunResult.StepResults.Add(new ScenarioStepResult
      {
        Duration = stopWatch.Elapsed,
        ScenarioStepResultType = ScenarioStepResultType.Inconclusive,
        LogMessages =
        {
          new TestResultLogMessage
          {
            Type = "Warning",
            Message = e.Message
          }
        }
      }.LoadStep(step));
    }

    private void HandleStepAsSkipped(ScenarioRunResult scenarioRunResult, StepForProcessing step, string messageOverride = null)
    {
      if (step == null) return;
      // we want to mark any step after a failure as inconclusive
      var message = messageOverride ?? $"  Skipping step '{step.Keyword}{step.Text}' as there was a previous failure";
      Log.Information(message);
      scenarioRunResult.StepResults.Add(new ScenarioStepResult
      {
        ScenarioStepResultType = ScenarioStepResultType.Skipped,
        LogMessages =
        {
          new TestResultLogMessage
          {
            Type = "Info",
            Message = message
          }
        }
      }.LoadStep(step));
    }

    private void HandleStepAsSkippedBecauseOfExecutionMode(ScenarioRunResult scenarioRunResult, StepForProcessing step)
    {
      var message = $"  Skipping step '{step.Keyword}{step.Text}' due to execution mode";
      HandleStepAsSkipped(scenarioRunResult, step, message);
    }

    private void LogResult()
    {
      if (inconclusive)
      {
        Log.Warning("##--------> Scenario result: inconclusive\n");
      }
      else if (passedSoFar)
      {
        Log.Information("##--------> Scenario result: passed\n");
      }
      else
      {
        Log.Error("##--------> Scenario result: failed\n");
      }
    }

    private void ProcessScenarioRunResult(ScenarioRunResult scenarioRunResult)
    {
      if (inconclusive)
      {
        scenarioRunResult.Result = ScenarioRunResultType.Inconclusive;
      }
      else
      {
        scenarioRunResult.Result = (passedSoFar) ? ScenarioRunResultType.Pass : ScenarioRunResultType.Failed;
      }
    }

    private bool CallAndFailScenarioOnException(Action act, string contextLocation, ScenarioRunResult scenarioRunResult)
    {
      try
      {
        act();

        return true;
      }
      catch (Exception e)
      {
        e.OutputException(TestSettings, $"    Failure in {contextLocation}: ");

        scenarioRunResult.Result = ScenarioRunResultType.Failed;
        scenarioRunResult.Errors.Add(new StepNotice{ NoticeCode = "Error", Message = e.Message });
        Log.Error("This will cause previously passing scenario(s) to now be marked as failure.");

        return false;
      }
    }
  }
}
