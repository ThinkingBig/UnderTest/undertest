using System.Collections.Generic;
using Gherkin.Ast;
using Examples = Gherkin.Ast.Examples;

namespace UnderTest.Infrastructure.ExampleTableDataSources
{
  public interface IExampleTableDataEnhancer
  {
    IUnderTestSettings TestSettings { get; }

    IList<Examples> AttemptToLoadFromScenario(Scenario scenario, FeatureContext featureContext);
  }
}
