using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using Gherkin.Ast;
using UnderTest.Exceptions;

namespace UnderTest.Infrastructure.ExampleTableDataSources
{
  public class FileExampleDataTableEnhancer : IExampleTableDataEnhancer, IHasTestSettings
  {
    public FileExampleDataTableEnhancer(TestRunSettings testSettings)
    {
      TestSettings = testSettings;
    }

    public IUnderTestSettings TestSettings { get; }

    public IList<Examples> AttemptToLoadFromScenario(Scenario scenario, FeatureContext featureContext)
    {
      return scenario.Examples.Select(examples => ProcessExampleTableForCsvDataSource(examples, featureContext)).ToList();
    }

    private Examples ProcessExampleTableForCsvDataSource(Examples example, FeatureContext featureContext)
    {
      var tag = example.Tags.FirstOrDefault(x => x.Name.StartsWith("@ExampleData:"));
      if (tag == null)
      {
        return example;
      }

      var filename = Path.Combine(TestSettings.AssetsFolder, tag.Name.Replace("@ExampleData:", string.Empty));
      if (!File.Exists(filename))
      {
        throw new ExampleDataFileDoesNotExistException($"Example data filename does not exist. Filename: {filename}")
          {Filename = filename};
      }

      if (example.TableBody.Any() && !TestSettings.ExecutionSettings.AllowInScenarioAndExternalExamples)
      {
        throw new MultipleExampleSourcesNotAllowedException(
          "The example table has both rows and an external datasource. To allow this call `.ConfigureExecutionSettings(x => x.SetAllowInScenarioAndExternalExamples(true)))` on `WithTestSettings` in `Program.cs`");
      }

      var orgCulture = CultureInfo.CurrentCulture.TextInfo.ListSeparator;
      try
      {
        CultureInfo.CurrentCulture.TextInfo.ListSeparator = new CultureInfo(featureContext.Document.Feature.Language, true).TextInfo.ListSeparator;

        var exampleRows = new List<TableRow>(example.TableBody);
        using (var reader = new StreamReader(filename))
        using (var csv = new CsvReader(reader))
        {
          csv.Configuration.HasHeaderRecord = true;

          csv.Read();
          csv.ReadHeader();
          while (csv.Read())
          {
            var exampleRow = BuildExampleTableRowFromCsv(example, csv);

            exampleRows.Add(exampleRow);
          }

          return BuildExamplesWithNewRows(example, exampleRows);
        }
      }
      finally
      {
        CultureInfo.CurrentCulture.TextInfo.ListSeparator = orgCulture;
      }
    }

    private static TableRow BuildExampleTableRowFromCsv(Examples example, CsvReader csv)
    {
      return new TableRow(null, cells:
        example.TableHeader.Cells.Select(headerCell =>
          new TableCell(null, csv.GetField(headerCell.Value))).ToArray());
    }

    private Examples BuildExamplesWithNewRows(Examples orgExample, IEnumerable<TableRow> updatedRows)
    {
      return new Examples(orgExample.Tags?.ToArray(), orgExample.Location, orgExample.Keyword, orgExample.Name,
        orgExample.Description, orgExample.TableHeader, updatedRows.ToArray());
    }
  }
}
