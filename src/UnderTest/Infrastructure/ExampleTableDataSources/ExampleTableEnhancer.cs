using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.Infrastructure.ExampleTableDataSources
{
  public class ExampleTableEnhancer : IExampleTableEnhancer
  {
    public ExampleTableEnhancer(TestRunSettings testRunSettings)
    {
      TestSettings = testRunSettings;
    }

    public IUnderTestSettings TestSettings { get; }

    public IList<Examples> EnhanceExamples(Scenario scenario, FeatureContext featureContext)
    {
      if (scenario == null)
      {
        return new List<Examples>();
      }

      if (!scenario.Examples.Any())
      {
        return new List<Examples>();
      }

      var enhancers = TestSettings.Container.GetAllInstances<IExampleTableDataEnhancer>();
      foreach (var enhancer in enhancers)
      {
        var examples = enhancer.AttemptToLoadFromScenario(scenario, featureContext);
        if (examples.Any())
        {
          return examples;
        }
      }

      return scenario.Examples.ToList();
    }
  }
}
