using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using SimpleInjector.Advanced;
using UnderTest.Attributes;

namespace UnderTest.Infrastructure.DI
{
  internal class PropertyInjectionPropertySelectionBehavior : IPropertySelectionBehavior
  {
    public bool SelectProperty(Type implementationType, PropertyInfo prop) =>
      prop.GetCustomAttributes(typeof(ImportAttribute)).Any()
      ||
      prop.GetCustomAttributes(typeof(PropertyInjectedAttribute)).Any()
      ||
      prop.GetCustomAttributes(typeof(InjectAttribute)).Any();
  }
}
