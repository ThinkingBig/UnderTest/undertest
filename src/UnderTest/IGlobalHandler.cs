using System;
using Serilog;
using UnderTest.Infrastructure;

namespace UnderTest
{
  [Obsolete("We are proposing that global handlers are an anti-pattern. This interface an associated functionality will be removed in a later version.")]
  public interface IGlobalHandler : IHasLogger, IHasTestSettings
  {
    new ILogger Log { get; }

    new IUnderTestSettings TestSettings { get; set; }
  }
}
