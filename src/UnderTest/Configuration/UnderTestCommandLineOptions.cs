using System.Collections.Generic;
using CommandLine;
using JetBrains.Annotations;

namespace UnderTest.Configuration
{
  [PublicAPI]
  public class UnderTestCommandLineOptions: IAmAConfigObjectMarker
  {
    [Option('o',"only-run-these-tags",

      Default = new string[] {},
      HelpText = "List of tags to run separated by commas",
      Separator = ',')]
    public IEnumerable<string> OnlyRunTheseTags { get; set; }

    [Option('d', "do-not-run-these-tags",
      Default = new string[] {},
      HelpText = "List of tags to ignore separated by commas",
      Separator = ',')]
    public IEnumerable<string> DoNotRunTheseTags { get; set; }

    [Option('r', "report-output-folder",
      Default = "",
      HelpText = "Path to the directory for outputted reports")]
    public string ReportOutputFolder { get; set; }

    [Option('f', "report-output-file-name",
      Default = "",
      HelpText = "File Name of the outputted report")]
    public string ReportOutputFileName { get; set; }
  }
}
