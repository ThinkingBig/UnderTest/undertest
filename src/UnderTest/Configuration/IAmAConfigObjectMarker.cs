using JetBrains.Annotations;

namespace UnderTest.Configuration
{
  [PublicAPI]
  public interface IAmAConfigObjectMarker
  { }
}
