using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CommandLine;
using JetBrains.Annotations;
using UnderTest.Exceptions;
using UnderTest.Infrastructure;

namespace UnderTest.Configuration
{
  [PublicAPI]
  internal class UnderTestCommandLineArgsParser
  {

    public UnderTestCommandLineArgsParser(string[] args, TestRunSettings testSettings)
    {
      this.args = args;
      this.testSettings = testSettings;
    }

    private IEnumerable<string> args;
    private readonly TestRunSettings testSettings;

    public void ApplyBuiltInArguments<TOptionsClass>(UnderTestRunner<TOptionsClass> runner)
       where TOptionsClass : UnderTestCommandLineOptions, new()
    {
      runner.TestSettings.CommandLineArgs = args?.ToArray();
      Parser.Default.ParseArguments<TOptionsClass>(args)
        .WithNotParsed(HandleInvalidParams)
        .WithParsed(HandleOptions(runner));
    }

    protected Action<UnderTestCommandLineOptions> HandleOptions<TOptionsClass>(UnderTestRunner<TOptionsClass> runner)
      where TOptionsClass: UnderTestCommandLineOptions, new()
    {
      return o =>
      {
        // add the parsed instance to the container
        testSettings.Container.RegisterInstance(typeof(TOptionsClass), o);

        // handle our default parameters
        if (o.OnlyRunTheseTags.Any())
        {
          o.OnlyRunTheseTags.ForEach(x => runner.TestSettings.Logger.Information($"Adding `{nameof(o.OnlyRunTheseTags)}` filter of `{x}`"));
          runner.TestSettings.OnlyRunTheseTags(o.OnlyRunTheseTags.ToArray());
        }

        if (o.DoNotRunTheseTags.Any())
        {
          o.DoNotRunTheseTags.ForEach(x => runner.TestSettings.Logger.Information($"Adding `{nameof(o.DoNotRunTheseTags)}` filter of `{x}`"));
          runner.TestSettings.DoNotRunTheseTags(o.DoNotRunTheseTags.ToArray());
        }

        if (!string.IsNullOrWhiteSpace(o.ReportOutputFolder))
        {
          var resolvedPath = Path.GetFullPath(o.ReportOutputFolder);
          runner.TestSettings.Logger.Information($"Setting `{nameof(TestRunOutputSettings.ReportOutputFolder)}` to {resolvedPath}");
          runner.TestSettings.OutputSettings.SetReportOutputFolder(resolvedPath);
        }

        if (!string.IsNullOrWhiteSpace(o.ReportOutputFileName))
        {
          runner.TestSettings.Logger.Information($"Setting `ReportOutputFileName` to {o.ReportOutputFileName}");
          runner.TestSettings.OutputSettings.SetReportOutputFileName(o.ReportOutputFileName);
        }
      };
    }

    protected virtual void HandleInvalidParams(IEnumerable<Error> errors)
    {
      var theErrors = errors.ToList();
      if(theErrors.Any(e => e is VersionRequestedError))
      {
        return;
      }

      throw new FailedToParseCommandLineParametersUnderTestException(
        $"Failed to parse args {string.Join("\n", theErrors)}") { Errors = theErrors};
    }
  }
}
