﻿using System;

// ReSharper disable once CheckNamespace
namespace UnderTest
{
  public static class FeatureContainerExecutionMode
  {
    internal static FeaturesContainer FilterBasedOnExecutionMode(this FeaturesContainer instance,
      TestRunSettings settings)
    {
      switch (settings.ExecutionMode)
      {
        case ExecutionMode.Normal:
        case ExecutionMode.OnlyExecuteGivens:
          return instance;
        default:
          throw new Exception($"Unknown execution mode {settings.ExecutionMode}");
      }
    }
  }
}
