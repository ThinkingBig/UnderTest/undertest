using System;
using UnderTest.Reporting;

// ReSharper disable once CheckNamespace
namespace UnderTest
{
  public static class UnderTestRunResultExtensions
  {
    /// <summary>
    /// Returns an <see cref="https://shapeshed.com/unix-exit-codes/"> exit code</see>.
    /// This enables calling processes, like CI servers, to know if a test passed without needing to probe the output report.
    /// </summary>
    /// <param name="instance">Result from an UnderTestRunner.Execute()</param>
    /// <returns>Exit code to be returned</returns>
    /// <exception cref="NullReferenceException">Throws null exception when the result is null.</exception>
    public static int ToExitCode(this UnderTestRunResult instance)
    {
      if (instance == null)
      {
        throw new NullReferenceException("ToExitCode() called on null UnderTest.Reporting");
      }

      switch (instance.OverallResult)
      {
        case TestRunOverallResult.Pass:
          return (int)UnderTestExitCodes.Pass;
        case TestRunOverallResult.Inconclusive:
          return (int)UnderTestExitCodes.Inconclusive;
        default:
          return (int)UnderTestExitCodes.Failure;
      }
    }
  }
}
