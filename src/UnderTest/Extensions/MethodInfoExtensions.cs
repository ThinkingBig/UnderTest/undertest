using System.Collections.Generic;
using System.Linq;
using UnderTest;
using UnderTest.Attributes;
using UnderTest.Behaviors;
using UnderTest.Exceptions;

// Extension methods go in target type's namespace
// ReSharper disable once CheckNamespace
namespace System.Reflection
{
  public static class MethodInfoExtensions
  {
    internal static List<BehaviorAttribute> GetBehaviors(this MethodInfo methodToCall, TestRunSettings testRunSettings)
    {
      var stepBehaviors = methodToCall
        .GetCustomAttributes(typeof(BehaviorAttribute), true)
        .Cast<BehaviorAttribute>()
        .ToList();
      stepBehaviors.ForEach(x => x.ApplyPropertyInjection(testRunSettings.Container));
      return stepBehaviors;
    }

    internal static bool HasExpectedFailureAttribute(this MethodInfo instance)
    {
      var attributes = instance?.GetCustomAttributes(typeof(ExpectedFailureAttribute), false);
      return attributes != null && attributes.Any();
    }

    internal static void ThrowIfInconclusive(this MethodInfo method)
    {
      var attributes = method.GetCustomAttributes(typeof(InconclusiveAttribute), false);
      if (!attributes.Any()) return;
      
      var message = $"Step marked inconclusive by attribute with message \"{attributes.Cast<InconclusiveAttribute>().Select(x => x.Message).FirstOrDefault() ?? string.Empty}\"";
      throw new MethodMarkedAsInconclusiveException(message) { Method = method };
    }
  }
}
