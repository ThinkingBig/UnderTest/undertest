using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnderTest.Infrastructure.StepParameters;

// ReSharper disable once CheckNamespace
namespace UnderTest.Locators
{
  // ReSharper disable once InconsistentNaming
  public static class IStepParameterExtractorExtensions
  {
    public static IList<StepParameter> FromRegExMatches(this IStepParameterExtractor instance,
      MatchCollection lambdaMatches)
    {
      var result = new List<StepParameter>();
      if (lambdaMatches == null)
      {
        return result;
      }

      foreach (Match match in lambdaMatches)
      {
        result.AddRange(match.Groups.ToList().Skip(1).Select(x => new StepParameter{ ParameterText = x.Value }));
      }

      return result;
    }
  }
}
