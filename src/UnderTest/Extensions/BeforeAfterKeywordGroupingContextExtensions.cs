// ReSharper disable once CheckNamespace

using System.Collections.Generic;
using UnderTest.Behaviors;

// ReSharper disable once CheckNamespace
namespace UnderTest.Strategies
{
  public static class BeforeAfterKeywordGroupingContextExtensions
  {
    public static void TriggerBeforeEvent(this BeforeAfterKeywordGroupingContext instance,
      ITestStrategy testStrategy, IFeatureHandler handler, TestRunSettings testRunSettings)
    {
      var context = new StepExecutingContext
      {
        Feature = instance.Feature,
        Handler = handler as FeatureHandler,
        Scenario = instance.Scenario
      };

      if (handler != null)
        testRunSettings.WrapMethodInBehaviors(handler.BeforeScenarioKeywordGrouping, context, instance);
      
      testStrategy.StrategyEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.BeforeBackgroundKeywordGrouping, context, instance));
      
      testRunSettings.TestRunEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.BeforeScenarioKeywordGrouping, context, instance));
    }

    public static void TriggerAfterEvent(this BeforeAfterKeywordGroupingContext instance,
      ITestStrategy testStrategy, IFeatureHandler handler, TestRunSettings testRunSettings)
    {
      var context = new StepExecutingContext
      {
        Feature = instance.Feature,
        Handler = handler as FeatureHandler,
        Scenario = instance.Scenario,
      };

      if (handler != null)
        testRunSettings.WrapMethodInBehaviors(handler.AfterBackgroundKeywordGrouping, context, instance);
      
      testStrategy.StrategyEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.AfterBackgroundKeywordGrouping, context, instance));
      
      testRunSettings.TestRunEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.AfterBackgroundKeywordGrouping, context, instance));
    }

    public static void TriggerBackgroundBeforeEvent(this BeforeAfterKeywordGroupingContext instance,
      ITestStrategy testStrategy, IFeatureHandler handler, TestRunSettings testRunSettings)
    {
      var context = new StepExecutingContext
      {
        Feature = instance.Feature,
        Handler = handler as FeatureHandler,
        Scenario = instance.Scenario,
      };
      
      testRunSettings.TestRunEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.BeforeBackgroundKeywordGrouping, context, instance));
      
      testStrategy.StrategyEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.BeforeBackgroundKeywordGrouping, context, instance));

      if (handler != null)
        testRunSettings.WrapMethodInBehaviors(handler.BeforeBackgroundKeywordGrouping, context, instance);
    }

    public static void TriggerBackgroundAfterEvent(this BeforeAfterKeywordGroupingContext instance,
      ITestStrategy testStrategy, IFeatureHandler handler, TestRunSettings testRunSettings)
    {
      var context = new StepExecutingContext
      {
        Feature = instance.Feature,
        Handler = handler as FeatureHandler,
        Scenario = instance.Scenario
      };

      if (handler != null)
        testRunSettings.WrapMethodInBehaviors(handler.AfterBackgroundKeywordGrouping, context, instance);
      
      testStrategy.StrategyEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.AfterBackgroundKeywordGrouping, context, instance));
      
      testRunSettings.TestRunEventHandlers.ForEach(x => 
        testRunSettings.WrapMethodInBehaviors(x.AfterBackgroundKeywordGrouping, context, instance));
    }
  }
}
