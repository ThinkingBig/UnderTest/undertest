using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnderTest.Attributes;
using UnderTest.Infrastructure.CrossPlatform;

// ReSharper disable once CheckNamespace
namespace UnderTest
{
  internal static class FeatureContainerDebugAttributesExtensions
  {
    internal static FeaturesContainer FilterBasedOnDebugAttributes(this FeaturesContainer instance, TestRunSettings settings)
    {
      // only apply DebugAttributes in Debug assemblies
      if (!settings.Assemblies.Any(x => x.IsAssemblyDebugBuild()))
      {
        settings.Logger.Information($"No debug assemblies detected.");
        return instance;
      }

      settings.Logger.Information($"Debug assembly detected. Running [DebugRunThis] filter.");
      var container = instance.FilterBasedOnDebugRunThisAttribute(settings, out var updatedContainer);
      if (updatedContainer)
      {
        settings.Logger.Information(string.Empty);
        return container;
      }

      settings.Logger.Information($"No [DebugRunThis] found. Running [DebugIgnoreThis] filter.");

      return instance.FilterBasedOnDebugIgnoreThisAttribute(settings);
    }

    private static FeaturesContainer FilterBasedOnDebugRunThisAttribute(this FeaturesContainer instance, TestRunSettings settings,
      out bool updatedFeatureList)
    {
      updatedFeatureList = false;
      var featureFileNamesToRun = FilterFeaturesBasedOnDebugAttribute<DebugRunThisAttribute>(instance, settings)
        .ToList();

      if (!featureFileNamesToRun.Any())
      {
        return instance;
      }

      settings.Logger.Information($"Found {featureFileNamesToRun.Count()} FeatureHandlers marked with `[DebugRunThis]`");
      // now update our feature file list to only incl4ude those
      var updatedFeatureContainer = new FeaturesContainer
      {
        Testable = new FeatureContextList(instance.Testable
          .Where(x => featureFileNamesToRun.Contains(x.Filename, new CrossPlatformFilePathEqualityComparer()))
          .ToList(), settings),
        Ignored = new FeatureContextList(settings),
        Wip = new FeatureContextList(settings)
      };

      if (instance.Testable.Count != updatedFeatureContainer.Testable.Count)
      {
        updatedFeatureList = true;
        settings.Logger.Warning($"[DebugRunThis] detected, updating testable feature list.");
        return updatedFeatureContainer;
      }

      settings.Logger.Information($"No [DebugRunThis] marked classes found.");

      return instance;
    }

    private static FeaturesContainer FilterBasedOnDebugIgnoreThisAttribute(this FeaturesContainer instance, TestRunSettings settings)
    {
      var featureFileNamesToIgnore = FilterFeaturesBasedOnDebugAttribute<DebugIgnoreThisAttribute>(instance, settings);

      // now update our feature file list to only include those
      var updatedFeatureContainer = new FeaturesContainer
      {
        Testable = new FeatureContextList(instance.Testable
          .Where(x => !featureFileNamesToIgnore.Contains(x.Filename))
          .ToList(), settings),
        Ignored = instance.Ignored,
        Wip = instance.Wip
      };

      updatedFeatureContainer
        .Ignored
        .AddRange(instance.Testable.Where(x => featureFileNamesToIgnore.Contains(x.Filename))
          .ToList());

      if (instance.Testable.Count != updatedFeatureContainer.Testable.Count)
      {
        settings.Logger.Information($"[DebugIgnoreThisAttribute] detected, updating testable feature list.");
        return updatedFeatureContainer;
      }

      settings.Logger.Information($"No [DebugIgnoreThisAttribute] marked classes found.");

      return instance;
    }

    private static IEnumerable<string> FilterFeaturesBasedOnDebugAttribute<T>(FeaturesContainer container, TestRunSettings settings)
      where T : IAmADebugAttribute
    {
      // find all of the FeatureHandlers with a DebugRunThisAttribute and select the FeatureFileNae
      return settings.Assemblies
        .Where(x => x.IsAssemblyDebugBuild())
        .SelectMany(x => x.GetTypes().Where(type => typeof(FeatureHandler).IsAssignableFrom(type)))
        .Where(type =>
          type
            .GetCustomAttributes(typeof(T), true)
            .Cast<T>()
            .Any())
        .SelectMany(x =>
          x
            .GetCustomAttributes(typeof(HandlesFeatureAttribute), true)
            .Cast<HandlesFeatureAttribute>()
            .Select(attr => attr.FeatureFileName)
        )
        .ToList();
    }
  }
}
