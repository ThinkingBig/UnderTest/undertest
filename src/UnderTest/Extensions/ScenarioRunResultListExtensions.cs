using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using UnderTest.Infrastructure;

// ReSharper disable once CheckNamespace
namespace UnderTest.Reporting
{
  public static class ScenarioRunResultListExtensions
  {
    public static void AddScenarioAsFullySkipped(this IList<ScenarioRunResult> instance, Scenario scenario,
      IEnumerable<StepForProcessing> backgroundSteps)
    {
      instance.Add(scenario.BuildIgnoredScenarioRunResultFromScenario(backgroundSteps));
    }

    public static void AddExampleScenariosAsSkipped(this IList<ScenarioRunResult> instance, Scenario scenario,
      IEnumerable<StepForProcessing> backgroundSteps, Examples examples)
    {
      examples?.TableBody?.ForEach(x => instance.AddScenarioAsFullySkipped(scenario, backgroundSteps));
    }
    
    public static void AddExampleScenariosAsIgnored(this IList<ScenarioRunResult> instance, Scenario scenario,
      IEnumerable<StepForProcessing> backgroundSteps, Examples examples)
    {
      examples?.TableBody?.ForEach(x => instance.AddScenarioAsFullyIgnored(scenario, backgroundSteps));
    }

    public static void AddScenarioAsFullyIgnored(this IList<ScenarioRunResult> instance, Scenario scenario,
      IEnumerable<StepForProcessing> backgroundSteps)
    {
      var steps = backgroundSteps.Select(x => new ScenarioStepResult
      {
        ScenarioStepResultType = ScenarioStepResultType.Skipped
      }.LoadStep(x)).ToList();

      steps.AddRange(scenario.Steps.Select(x => new ScenarioStepResult
      {
        ScenarioStepResultType = ScenarioStepResultType.Skipped
      }.LoadStep(new StepForProcessing(x))).ToList());

      instance.Add(new ScenarioRunResult
      {
        ScenarioName = scenario.Name,
        Result = ScenarioRunResultType.Ignored,
        Keyword = scenario.Keyword,
        Description = scenario.Description,
        StepResults = steps
      });
    }

    public static void AddScenarioAsFullyFailed(this IList<ScenarioRunResult> instance, Scenario scenario,
      IEnumerable<StepForProcessing> backgroundSteps)
    {
      var steps = backgroundSteps.Select(x => new ScenarioStepResult
      {
        ScenarioStepResultType = ScenarioStepResultType.Skipped
      }.LoadStep(x)).ToList();

      steps.AddRange(scenario.Steps.Select(x => new ScenarioStepResult
      {
        ScenarioStepResultType = ScenarioStepResultType.Skipped
      }.LoadStep(new StepForProcessing(x))).ToList());

      instance.Add(new ScenarioRunResult
      {
        ScenarioName = scenario.Name,
        Result = ScenarioRunResultType.Failed,
        Keyword = scenario.Keyword,
        Description = scenario.Description,
        StepResults = steps
      });
    }

    public static void AddScenarioAsFullyWip(this IList<ScenarioRunResult> instance, Scenario scenario,
      IEnumerable<StepForProcessing> backgroundSteps)
    {
      var steps = backgroundSteps.Select(x => new ScenarioStepResult
      {
        Duration = TimeSpan.Zero,
        ScenarioStepResultType = ScenarioStepResultType.Wip
      }.LoadStep(x)).ToList();

      steps.AddRange(scenario.Steps.Select(x => new ScenarioStepResult
      {
        Duration = TimeSpan.Zero,
        ScenarioStepResultType = ScenarioStepResultType.Wip
      }.LoadStep(new StepForProcessing(x))).ToList());

      instance.Add(new ScenarioRunResult
      {
        ScenarioName = scenario.Name,
        Result = ScenarioRunResultType.Wip,
        Keyword = scenario.Keyword,
        Description = scenario.Description,
        StepResults = steps
      });
    }
  }
}
