using System.Collections.Generic;
using System.Linq;
using UnderTest.Infrastructure;
using UnderTest.Reporting;

namespace Gherkin.Ast
{
  public static class ScenarioExtensions
  {
    public static ScenarioRunResult BuildIgnoredScenarioRunResultFromScenario(this Scenario instance,
      IEnumerable<StepForProcessing> backgroundSteps)
    {
      var steps = backgroundSteps.Select(x => new ScenarioStepResult
      {
        ScenarioStepResultType = ScenarioStepResultType.Skipped
      }.LoadStep(x)).ToList();

      steps.AddRange(instance.Steps.Select(x => new ScenarioStepResult
      {
        ScenarioStepResultType = ScenarioStepResultType.Skipped
      }.LoadStep(new StepForProcessing(x))).ToList());

      return new ScenarioRunResult
      {
        ScenarioName = instance.Name,
        Result = ScenarioRunResultType.Ignored,
        Keyword = instance.Keyword,
        Description = instance.Description,
        StepResults = steps
      };
    }

    public static bool IsScenarioOutline(this Scenario instance)
    {
      return instance != null
             && ("Scenario Outline".Equals(instance.Keyword) || "Scenario Template".Equals(instance.Keyword));
    }
  }
}
