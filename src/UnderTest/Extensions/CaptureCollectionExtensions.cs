using System.Collections.Generic;
using JetBrains.Annotations;

// ReSharper disable once CheckNamespace
namespace System.Text.RegularExpressions
{
  [PublicAPI]
  public static class CaptureCollectionExtensions
  {
    public static IList<Capture> ToList(this CaptureCollection instance)
    {
      var result = new List<Capture>();
      if (instance == null)
      {
        return result;
      }

      foreach (Capture capture in instance)
      {
        result.Add(capture);
      }

      return result;
    }
  }
}
