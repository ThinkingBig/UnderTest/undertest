namespace UnderTest
{
  public class FeaturesContainer
  {
    public FeatureContextList Testable { get; set; }

    public FeatureContextList Ignored { get; set; }

    public FeatureContextList Wip { get; set; }
  }
}
