using Gherkin.Ast;
using JetBrains.Annotations;
using UnderTest.Infrastructure;
using UnderTest.Reporting;

namespace UnderTest
{
  [PublicAPI]
  public class BeforeAfterKeywordGroupingContext
  {
    public FeatureContext Feature { get; set; }

    public ScenarioKeywordGrouping KeywordGrouping { get; set; }

    public Scenario Scenario { get; set; }

    public ScenarioRunResult ScenarioRunResult { get; set; }
  }
}
