using Gherkin.Ast;
using JetBrains.Annotations;
using UnderTest.Infrastructure;
using UnderTest.Reporting;

namespace UnderTest
{
  [PublicAPI]
  public class BeforeAfterStepContext
  {
    public FeatureContext Feature { get; set; }

    public Scenario Scenario { get; set; }

    public StepForProcessing Step { get; set; }

    public ScenarioStepResult ScenarioStepResult { get; set; }
  }
}
