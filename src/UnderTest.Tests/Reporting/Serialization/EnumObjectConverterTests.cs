using System.Collections.Generic;
using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using UnderTest.Reporting.Serialization;

namespace UnderTest.Tests.Reporting.Serialization
{
  public class EnumObjectConverterTests
  {
    [Test]
    public void Serialize_WhenCalled_Deserializes()
    {
      var value = new TestClass
      {
        Test = TestEnum.OtherValue,
        Something = "Something"
      };
      var json = JsonConvert.SerializeObject(value);

      var result = JsonConvert.DeserializeObject<TestClass>(json);

      result.Test.Should().Be(value.Test);
      result.Something.Should().Be(value.Something);
    }

    [Test]
    public void Serialize_WhenNull_ReturnsNull()
    {
      var value = new TestClass
      {
        Test = null,
        Something = "Something"
      };
      var json = JsonConvert.SerializeObject(value);

      var result = JsonConvert.DeserializeObject<TestClass>(json);

      result.Test.Should().Be(null);
      result.Something.Should().Be(value.Something);
    }

    private class TestClass
    {
      [JsonConverter(typeof(EnumObjectConverter<TestEnum>))]
      public TestEnum? Test { get; set; }

      public string Something { get; set; }
    }

    private enum TestEnum
    {
      Default = 1,
      OtherValue = 2
    }
  }
}
