using FluentAssertions;
using NUnit.Framework;
using UnderTest.Reporting;

namespace UnderTest.Tests.Reporting
{
  public class TestRunFeatureStrategyResultTests
  {
    [Test]
    public void Constructor_WhenCalled_PropertiesAreNonNull()
    {
      var instance = new TestRunFeatureStrategyResult();

      instance.ScenarioTestResults.Should().NotBeNull();
    }
  }
}