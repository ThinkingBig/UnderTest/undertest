using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Reporting;

namespace UnderTest.Tests.Reporting
{
  public class TestRunExecutionTimeTests
  {
    [Test]
    public void Constructor_WhenCalled_CreatesInstance()
    {
      var instance = new TestRunExecutionTime();

      instance.StartTime.Should().NotBe(default(DateTime));
    }

    [Test]
    public void Duration_WhenStartEndAreNotSet_ReturnZero()
    {
      var instance = new TestRunExecutionTime
      {
        StartTime = default(DateTime),
        EndTime = default(DateTime)
      };


      instance.Duration.Should().Be(TimeSpan.Zero);
    }

    [Test]
    public void Duration_WhenStartAndEndAreSet_DurationIsReturned()
    {
      var difference = TimeSpan.FromMinutes(1);
      var anchor = DateTime.Now;
      var instance = new TestRunExecutionTime
      {
        StartTime = anchor,
        EndTime = anchor.Add(difference)
      };

      instance.Duration.Should().Be(difference);
    }
  }
}
