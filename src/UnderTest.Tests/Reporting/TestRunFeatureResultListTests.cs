using System;
using System.IO;
using System.Reflection;
using FluentAssertions;
using Gherkin.Ast;
using NUnit.Framework;
using UnderTest.Reporting;

namespace UnderTest.Tests.Reporting
{
  public class TestRunFeatureResultListTests
  {
    private const string SimpleFeatureName = "this is valid";
    private const string SimpleFeatureFileName = "test.feature";

    [Test]
    public void EnsureFeatureInTestResultAndReturn_WhenEmpty_AddsNewItem()
    {
      var instance = new TestRunFeatureResultList();
      var featureContext = new FeatureContext()
      {
        Filename = SimpleFeatureFileName,
        Document = LoadSimpleDocument()
      };

      var featureResult = instance.EnsureFeatureInTestResultAndReturn(featureContext);
      instance.Count.Should().Be(1);

      featureResult.Name.Should().Be(SimpleFeatureName);
      featureResult.FeatureFileName.Should().Be(SimpleFeatureFileName);
    }

    [Test]
    public void EnsureFeatureInTestResultAndReturn_WhenCalledWithTheSameContext_ReturnsExistingInstance()
    {
      var instance = new TestRunFeatureResultList();
      var featureContext = new FeatureContext()
      {
        Filename = SimpleFeatureFileName,
        Document = LoadSimpleDocument()
      };

      instance.EnsureFeatureInTestResultAndReturn(featureContext);
      var featureResult = instance.EnsureFeatureInTestResultAndReturn(featureContext);

      instance.Count.Should().Be(1);

      featureResult.Name.Should().Be(SimpleFeatureName);
      featureResult.FeatureFileName.Should().Be(SimpleFeatureFileName);
    }

    [Test]
    public void EnsureFeatureInTestResultAndReturn_FeatureContextIsNull_ThrowException()
    {
      var instance = new TestRunFeatureResultList();

      Assert.Throws<ArgumentNullException>(() => instance.EnsureFeatureInTestResultAndReturn(null));
    }

    [Test]
    public void EnsureFeatureInTestResultAndReturn_WhenDocumentIsNull_ThrowException()
    {
      var instance = new TestRunFeatureResultList();

      var featureContext = new FeatureContext
      {
        Filename = "test.feature"
      };

      Assert.Throws<ArgumentException>(() => instance.EnsureFeatureInTestResultAndReturn(featureContext));
    }

    private GherkinDocument LoadSimpleDocument()
    {
      const string simpleValidFeature = "Feature: " + SimpleFeatureName;

      using(var sr = new StringReader(simpleValidFeature))
      {
        return new Gherkin.Parser().Parse(sr);
      }
    }
  }
}
