using FluentAssertions;
using NUnit.Framework;
using UnderTest.Reporting;

namespace UnderTest.Tests.Reporting
{
  public class ScenarioRunResultTests
  {
    [Test]
    public void Constructor_WhenCalled_PropertiesAreNonNull()
    {
      var instance = new ScenarioRunResult();

      instance.StepResults.Should().NotBeNull();
    }
  }
}