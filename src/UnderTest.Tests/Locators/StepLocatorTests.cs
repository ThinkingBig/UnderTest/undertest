using System;
using FluentAssertions;
using Gherkin.Ast;
using NUnit.Framework;
using UnderTest.Infrastructure;
using UnderTest.Locators;
using UnderTest.Tests.TestSupportingClasses.ExampleFeatureHandlers;

namespace UnderTest.Tests.Locators
{
  public class StepLocatorTests
  {
    [Test]
    public void Constructor_PassedNull_Throws()
    {
      Action act = () => new StepLocator(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void LocateStep_PassedNull_ThrowsArgumentNullException()
    {
      var settings = new TestRunSettings();
      var instance = new StepLocator(settings);

      Action act = () => instance.LocateStep(null, null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void LocateStep_WhenCacheIsInvalid_ThrowsArgumentException()
    {
      var settings = new TestRunSettings();
      var instance = new StepLocator(settings);

      var step = new Step(null, "Given", "I am a step that matches", null);
      var stepForProcessing = new StepForProcessing(step);
      Action act = () => instance.LocateStep(stepForProcessing, null);

      act.Should().Throw<ArgumentException>();
    }

    [Test]
    public void LocateStep_PassedMatchingStep_ReturnsSingleStep()
    {
      const string expectedMethodName = "IAmAStepThatMatches";
      var settings = BuildBasicValidTestRunSettings();
      var instance = new StepLocator(settings);

      var step = new Step(null, "Given", "I am a step that matches", null);
      var stepForProcessing = new StepForProcessing(step);
      var result = instance.LocateStep(stepForProcessing, new SimpleValidExampleFeatureHandler());

      result.Should().NotBeNull("Step should been matched.");
      result.MethodToCall.Name.Should().Be(expectedMethodName);
    }

    [Test]
    public void LocateStep_PassedPartiallyMatchingStep_DoesNotReturnStep()
    {
      var settings = BuildBasicValidTestRunSettings();
      var instance = new StepLocator(settings);

      var partialStep = new Step(null, "Given", "Partial match of a step", null);
      var partialStepForProcessing = new StepForProcessing(partialStep);

      var result = instance.LocateStep(partialStepForProcessing, new SimpleValidExampleFeatureHandler());

      result.Should().BeNull();
    }

    [Test]
    public void LocateStep_PassedMatchingStepBasedOnRegex_ReturnsSingleStep()
    {
      const string expectedMethodName = "IAmAStepThatMatchesBasedOnRegex";
      var settings = BuildBasicValidTestRunSettings();
      var instance = new StepLocator(settings);

      var step = new Step(null, "Given", "I click two times", null);
      var stepForProcessing = new StepForProcessing(step);
      var result = instance.LocateStep(stepForProcessing, new SimpleValidExampleFeatureHandler());

      result.Should().NotBeNull("Step should been matched.");
      var method = result.MethodToCall;
      method.Name.Should().Be(expectedMethodName);

      result.StepParameters.Count.Should().Be(1);
    }

    [Test]
    public void LocateStep_PassedMatchingStepBasedOnRegexWithStepValue_ReturnsSingleStep()
    {
      const string expectedMethodName = "IAmAStepThatMatchesBasedOnRegex";
      var settings = BuildBasicValidTestRunSettings();
      var instance = new StepLocator(settings);

      var step = new Step(null, "Given", "I click <num-times> times", null);
      var stepForProcessing = new StepForProcessing(step)
      {
        ExampleTableRow = new ExampleTableRow(
          new []{ new TableCell(new Location(), "num-times") },
          new []{ new TableCell(new Location(), "two") }
          )
      };
      var result = instance.LocateStep(stepForProcessing, new ValueBasedMatcherExampleFeatureHandler());

      result.Should().NotBeNull("Step should been matched.");
      var method = result.MethodToCall;
      method.Name.Should().Be(expectedMethodName);

      result.StepParameters.Count.Should().Be(1);
    }

    private static TestRunSettings BuildBasicValidTestRunSettings()
    {
      var settings = new TestRunSettings();
      settings.AddAssembly(typeof(StepLocatorTests).Assembly);
      settings.TypesCache.BuildTypeCache(settings);

      return settings;
    }
  }
}
