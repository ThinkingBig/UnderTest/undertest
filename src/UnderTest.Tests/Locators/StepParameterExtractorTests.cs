using System;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Infrastructure.StepParameters;
using UnderTest.Locators;

namespace UnderTest.Tests.Locators
{
  public class StepParameterExtractorTests
  {
    [Test]
    public void Extract_PassedNull_ThrowsANE()
    {
      var instance = new StepParameterExtractor(new TestRunSettings());

      Action act = () => instance.Extract(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void Extract_PassedStepWithNoParameters_ReturnsNoMatchedParameters()
    {
      var instance = new StepParameterExtractor(new TestRunSettings());

      var result = instance.Extract("Something is true");

      result.Any().Should().BeFalse();
    }

    [Test]
    public void Extract_PassedMatchableParameter_ReturnsMatchedParameter()
    {
      var instance = new StepParameterExtractor(new TestRunSettings());

      var result = instance.Extract("Something <something>");

      result.Any().Should().BeTrue();
      result.First().ParameterText = "<something>";
    }

    [Test]
    public void Extract_PassedMatchableParameters_ReturnsMatchedParameters()
    {
      var instance = new StepParameterExtractor(new TestRunSettings());

      var result = instance.Extract("Something <result1> <result2>");

      result.Any().Should().BeTrue();
      result.First().ParameterText.Should().Be("<result1>");
      result.Last().ParameterText.Should().Be("<result2>");
    }
  }
}
