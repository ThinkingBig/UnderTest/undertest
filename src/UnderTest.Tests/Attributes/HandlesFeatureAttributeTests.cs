using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Attributes;

namespace UnderTest.Tests.Attributes
{
  public class HandlesFeatureAttributeTests
  {
    [Test]
    public void Constructor_PassedNull_ThrowsArgumentNullException()
    {
      Action act = () => new HandlesFeatureAttribute(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void Constructor_PassedFeatureFileName_FeatureFileNameSet()
    {
      var expected = "foo.feature";

      var instance = new HandlesFeatureAttribute(expected);

      instance.FeatureFileName.Should().Be(expected);
    }
  }
}
