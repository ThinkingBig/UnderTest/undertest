using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Attributes;

namespace UnderTest.Tests.Attributes
{
  public class StepAttributeTests
  {
    [Test]
    public void StepAttribute_ConstructorPassedNull_Throws()
    {
      Action act = () => new StepAttribute(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void StepAttribute_ConstructorPassedStep_StepIsSet()
    {
      var expected = "shoes";
      var instance = new StepAttribute(expected);

      instance.Step.Should().Be(expected);
    }
  }
}
