using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Attributes.StepParams;
using UnderTest.Exceptions;

namespace UnderTest.Tests.Attributes.StepParams
{
  public class RelativeDateStepParamAttributeTests
  {
    [Test]
    public void Transform_PassedToday_ReturnsToday()
    {
      var instance = new RelativeDateStepParamAttribute();

      var result = instance.Transform("Today");

      result.Should().Be(DateTime.UtcNow.Date);
    }

    [Test]
    public void Transform_PassedTomorrow_ReturnsToday()
    {
      var instance = new RelativeDateStepParamAttribute();

      var result = instance.Transform("Tomorrow");

      result.Should().Be(DateTime.UtcNow.Date.AddDays(1));
    }

    [Test]
    public void Transform_PassedYesterday_ReturnsToday()
    {
      var instance = new RelativeDateStepParamAttribute();

      var result = instance.Transform("Yesterday");

      result.Should().Be(DateTime.UtcNow.Date.Subtract(TimeSpan.FromDays(1)).Date);
    }

    [Test]
    public void Transform_PassedUnknownValue_Throws()
    {
      var instance = new RelativeDateStepParamAttribute();

      Action act = () => instance.Transform("tuesday");

      act.Should().Throw<UnsupportedRelativeTimeStepParamValueException>();
    }
  }
}
