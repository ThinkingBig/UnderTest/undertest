using FluentAssertions;
using NUnit.Framework;
using UnderTest.Attributes.StepParams;

namespace UnderTest.Tests.Attributes.StepParams
{
  public class YesNoStepParamAttributeTests
  {
    [Test]
    public void Transform_PassedYes_ReturnsTrue()
    {
      var instance = new YesNoStepParamAttribute();

      var result = instance.Transform("Yes");

      result.Should().Be(true);
    }

    [Test]
    public void Transform_PassedNo_ReturnsTrue()
    {
      var instance = new YesNoStepParamAttribute();

      var result = instance.Transform("No");

      result.Should().Be(false);
    }

    [Test]
    public void Transform_PassedNull_ReturnsTrue()
    {
      var instance = new YesNoStepParamAttribute();

      var result = instance.Transform(null);

      result.Should().Be(false);
    }
  }
}
