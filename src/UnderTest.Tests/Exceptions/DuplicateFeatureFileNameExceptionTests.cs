using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Exceptions;

namespace UnderTest.Tests.Exceptions
{
  public class DuplicateFeatureFileNameExceptionTests
  {
    [Test]
    public void DefaultConstructor_PassedMessage_MessageIsSet()
    {
      const string message = "shoes";

      var instance = new DuplicateFeatureFileNameException(message);

      instance.Message.Should().Be(message);
      instance.InnerException.Should().BeNull();
    }

    [Test]
    public void Class_Serialization_Deserializes()
    {
      const string message = "shoes";
      const string filename = "test.file";

      var inner = new Exception("inner");
      var instance = new DuplicateFeatureFileNameException(message, inner)
      {
        FeatureFileName = filename
      };

      var result = instance.Clone();

      result.FeatureFileName.Should().Be(instance.FeatureFileName);
      result.InnerException.Message.Should().Be(instance.InnerException.Message);
    }
  }
}
