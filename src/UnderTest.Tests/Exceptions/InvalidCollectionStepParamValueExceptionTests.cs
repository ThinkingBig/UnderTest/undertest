using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Exceptions;

namespace UnderTest.Tests.Exceptions
{
  public class InvalidCollectionStepParamValueExceptionTests
  {
    [Test]
    public void DefaultConstructor_PassedMessage_MessageIsSet()
    {
      const string message = "shoes";

      var instance = new MissingStepBindingException(message);

      instance.Message.Should().Be(message);
      instance.InnerException.Should().BeNull();
    }

    [Test]
    public void Class_Serialization_Deserializes()
    {
      const string TypeName = "Type.Int32";
      const string Value = "TEST VALUE";
      const string message = "something something";

      var inner = new Exception("inner");
      var instance = new InvalidCollectionStepParamValueException(message, inner)
      {
        Value = Value,
        ListCollectionTypeName = TypeName
      };

      var result = instance.Clone();

      result.Value.Should().Be(Value);
      result.ListCollectionTypeName.Should().Be(TypeName);
      result.InnerException.Message.Should().Be(instance.InnerException.Message);
    }
  }
}
