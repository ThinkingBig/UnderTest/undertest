using FluentAssertions;
using NUnit.Framework;
using UnderTest.Exceptions;

namespace UnderTest.Tests.Exceptions
{
  public class UnsupportedRelativeTimeStepParamValueExceptionTests
  {
    [Test]
    public void DefaultConstructor_PassedMessage_MessageIsSet()
    {
      const string message = "shoes";

      var instance = new UnsupportedRelativeTimeStepParamValueException(message);

      instance.Message.Should().Be(message);
      instance.InnerException.Should().BeNull();
    }
  }
}