using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using UnderTest.Arguments;

namespace UnderTest.Tests.Arguments
{
  public class GherkinDataTableRowTests
  {
    [Test]
    public void Constructor_WhenPassedRows_RowsPropSet()
    {
      const string headerName = "header";
      const string expected = "shoes";
      var header = new GherkinDataTableHeader( new List<GherkinDataTableCell>{ new GherkinDataTableCell(headerName) });
      var cells = new List<GherkinDataTableCell> { new GherkinDataTableCell(expected) };

      var result = new GherkinDataTableRow(header, cells);

      result.Cells.Count().Should().Be(1);
      result.Cells.First().Value.Should().Be(expected);
    }
  }
}
