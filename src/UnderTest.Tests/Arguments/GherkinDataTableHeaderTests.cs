using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Arguments;

namespace UnderTest.Tests.Arguments
{
  public class GherkinDataTableHeaderTests
  {
    [Test]
    public void Constructor_WhenCalledWithCells_CellsAreSet()
    {
      var expected = new List<GherkinDataTableCell> {new GherkinDataTableCell("shoes") };

      var instance = new GherkinDataTableHeader(expected);

      instance.Cells.Should().Equal(expected);
    }
  }
}
