using System;
using FluentAssertions;
using NUnit.Framework;

namespace UnderTest.Tests.Extensions
{
  public class ObjectExtensionTests
  {
    [Test]
    public void ThrowIfNull_PassedNull_ThrowsANE()
    {
      object instance = null;

      Action act = () => instance.ThrowIfNull(nameof(instance));

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void ThrowIfNull_PassNonNull_DoesntThrow()
    {
      var instance = 5;

      Action act = () => instance.ThrowIfNull(nameof(instance));

      act.Should().NotThrow();
    }
  }
}
