using System.Text.RegularExpressions;
using FluentAssertions;
using NUnit.Framework;

namespace UnderTest.Tests.Extensions
{
  public class MatchCollectionExtensionsTests
  {
    [Test]
    public void ToList_PassedNull_ReturnsEmptyList()
    {
      MatchCollection instance = null;

      var result = instance.ToList();

      result.Should().NotBeNull();
      result.Count.Should().Be(0);
    }

    [Test]
    public void ToList_PassedCollection_ReturnsList()
    {
      var matches = Regex.Matches("Morty", "(.*)");

      var result = matches.ToList();

      result.Should().NotBeNull();
      result.Count.Should().Be(2);
    }
  }
}
