using System;
using FluentAssertions;
using NUnit.Framework;

namespace UnderTest.Tests.Extensions
{
  public class StringExtensionTests
  {
    [Test]
    public void NormalizeFilePath_PassedNull_ThrowsANE()
    {
      string instance = null;

      // ReSharper disable once ExpressionIsAlwaysNull
      Action act = () => instance.NormalizeFilePath();

      act.Should().Throw<ArgumentNullException>();
    }

    [TestCase("something.feature", "something.feature", Description = "Basic filename, returns unchanged")]
    [TestCase("SOMETHING.FEATURE", "something.feature", Description = "Uppercase return lowercase")]
    [TestCase(@"Folder/Something.feature", @"folder/something.feature", Description = "Linux folder path - returns same lowercase" )]
    [TestCase(@"Folder\Something.feature", @"folder/something.feature", Description = "Windows folder path - returns Unix path lowercase" )]
    public void NormalizeFilePath_PassedCurrentFolderAllLowerCase_ReturnsInput(string input, string expected)
    {
      var instance = input;

      var result = instance.NormalizeFilePath();

      result.Should().Be(expected);
    }
  }
}
