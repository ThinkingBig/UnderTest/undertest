using FluentAssertions;
using Gherkin.Ast;
using NUnit.Framework;

namespace UnderTest.Tests.Extensions
{
  public class TableRowExtensionsTests
  {
    [Test]
    public void ToDisplayOfRowValues_WhenCalledWithNull_ReturnsEmpty()
    {
      TableRow instance = null;

      var result = instance.ToDisplayOfRowValues();

      result.Should().Be(string.Empty);
    }

    [Test]
    public void ToDisplayOfRowValues_WhenCalledWithSingleColumn_ReturnsColumnValue()
    {
      var value = "morty";
      var instance = new TableRow(null, new []{ new TableCell(null, value) });

      var result = instance.ToDisplayOfRowValues();
      result.Should().Be(value);
    }

    [Test]
    public void ToDisplayOfRowValues_WhenCalledWithMultipleValues_ReturnsSpaceDashSpaceDelimited()
    {
      const string value1 = "rick";
      const string value2 = "morty";
      const string expected = "rick - morty";

      var instance = new TableRow(null, new []{ new TableCell(null, value1), new TableCell(null, value2) });

      var result = instance.ToDisplayOfRowValues();
      result.Should().Be(expected);
    }
  }
}
