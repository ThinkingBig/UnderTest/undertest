using System.Collections.Generic;
using FluentAssertions;
using Gherkin.Ast;
using NUnit.Framework;
using UnderTest.Infrastructure;

namespace UnderTest.Tests.Infrastructure
{
  public class ExampleTableRowTests
  {
    [Test]
    public void Constructor_PassedValues_SetsHeaderAndRows()
    {
      var header = new List<TableCell>();
      var row = new List<TableCell>();
      var result = new ExampleTableRow(header, row);

      result.HeaderCells.Should().BeEquivalentTo(header);
      result.Row.Should().BeEquivalentTo(row);
    }

    [Test]
    public void ApplyRowToStepText_WhenCalledWithNoRow_ReturnsOrgStep()
    {
      const string step = "I am a step";

      var instance = new ExampleTableRow(null, null);

      var result = instance.ApplyRowToStepText(step);

      result.Should().BeEquivalentTo(step);
    }

    [Test]
    public void ApplyRowToStepText_WhenCalledWithOneValue_ReturnsUpdatedStep()
    {
      const string step = "I am a step with a <value>";
      const string expected = "I am a step with a shoes";
      var location = new Location(0, 0);

      var instance = new ExampleTableRow(
        new []{ new TableCell(location, "value") },
        new []{ new TableCell(location, "shoes") });

      var result = instance.ApplyRowToStepText(step);

      result.Should().BeEquivalentTo(expected);
    }

    [Test]
    public void ApplyRowToStepText_WhenCalledWithMultipleValues_ReturnsUpdatedStep()
    {
      const string step = "I am a step with a <value1> <value2>";
      const string expected = "I am a step with a cool shoes";
      var location = new Location();

      var instance = new ExampleTableRow(
        new []
        {
          new TableCell(location, "value1"),
          new TableCell(location, "value2")
        },
        new []
        {
          new TableCell(location, "cool"),
          new TableCell(location, "shoes")
        });

      var result = instance.ApplyRowToStepText(step);

      result.Should().BeEquivalentTo(expected);
    }
  }
}
