﻿using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.Infrastructure.Binding;
using UnderTest.Support;

namespace UnderTest.Tests.Infrastructure.Binding
{
  public class UnderTestEnumBinderTests
  {
    [Test]
    [TestCase(typeof(Basic), @"Foo", Basic.Foo)]    // full match
    [TestCase(typeof(Basic), @"Foo ", Basic.Foo)]   // trim needed
    [TestCase(typeof(Basic), @"foo ", Basic.Foo)]   // ToLower needed
    [TestCase(typeof(Basic), @" foo ", Basic.Foo)]  // trim on both sides needed
    [TestCase(typeof(WithAliases), @"Foo", WithAliases.Foo)]    // full match
    [TestCase(typeof(WithAliases), @"Foo ", WithAliases.Foo)]   // trim needed
    [TestCase(typeof(WithAliases), @"foo ", WithAliases.Foo)]   // ToLower needed
    [TestCase(typeof(WithAliases), @" foo ", WithAliases.Foo)]  // trim on both sides needed
    [TestCase(typeof(WithMultiAliases), @"Foo", WithMultiAliases.Foo)]    // full match
    [TestCase(typeof(WithMultiAliases), @"Foo ", WithMultiAliases.Foo)]   // trim needed
    [TestCase(typeof(WithMultiAliases), @"foo ", WithMultiAliases.Foo)]   // ToLower needed
    [TestCase(typeof(WithMultiAliases), @" foo ", WithMultiAliases.Foo)]  // trim on both sides needed
    public void ToEnum_SuccessCases(Type targetType, string value, object expected)
    {
      var result = UnderTestEnumBinder.ToEnum(targetType, value);

      result.Should().Be(expected);
    }

    private enum Basic
    {
      Foo
    }

    private enum WithAliases
    {
      [EnumAlias("Foo")]
      Foo
    }

    private enum WithMultiAliases
    {
      [EnumAlias("Not foo")]
      [EnumAlias("Foo")]
      Foo
    }
  }
}
