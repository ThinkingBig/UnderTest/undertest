using System.Diagnostics.CodeAnalysis;
using UnderTest.Infrastructure;
using UnderTest.Reporting;

namespace UnderTest.Tests.TestSupportingClasses.TestRunSettingsTypes
{
  public class NullReportOutputWriter: IReportOutputWriter
  {
    public IUnderTestSettings TestSettings { get; }

    public void SaveResults(UnderTestRunResult results)
    {
      // noop
    }
  }
}
