using System.Diagnostics.CodeAnalysis;
using UnderTest.Locators;

namespace UnderTest.Tests.TestSupportingClasses.TestRunSettingsTypes
{
  public class NullFeatureHandlerLocator : IFeatureHandlerLocator
  {
    public NullFeatureHandlerLocator(TestRunSettings settings)
    {
      TestSettings = settings;
    }

    public string Name { get; } = "NullFeatureHandlerLocator";

    public TestRunSettings TestSettings { get; }

    public IFeatureHandler LocateContainerByFeatureContext(FeatureContext feature)
    {
      return null;
    }
  }
}
