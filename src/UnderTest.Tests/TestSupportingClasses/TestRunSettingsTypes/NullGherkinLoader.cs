using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Serilog;
using UnderTest.Exceptions;
using UnderTest.Infrastructure;

namespace UnderTest.Tests.TestSupportingClasses.TestRunSettingsTypes
{
  public class NullGherkinLoader : IGherkinLoader
  {
    public NullGherkinLoader(TestRunSettings settings)
    {
      TestSettings = settings;
    }

    public ILogger Log => TestSettings.Logger;

    public IUnderTestSettings TestSettings { get; }

    public (FeatureContextList, IList<UnderTestFailedToParseFeatureFilesException>) LoadAllFromFolder(string targetFolder)
    {
      return (new FeatureContextList(TestSettings), new List<UnderTestFailedToParseFeatureFilesException>());
    }
  }
}
