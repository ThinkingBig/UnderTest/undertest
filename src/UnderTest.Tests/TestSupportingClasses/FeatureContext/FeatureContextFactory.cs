using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.Tests.TestSupportingClasses.FeatureContextRelated
{
  internal class FeatureContextFactory
  {
    public FeatureContext GenerateFeatureContextWithNumberOfTags(int numFeatureTags, Func<string> tagTextGenerator)
    {
      var tags = GenerateTagsWithText(numFeatureTags, tagTextGenerator);

      var feature = new Feature(tags.ToArray(), new Location(0,0), string.Empty, string.Empty, string.Empty, string.Empty, new IHasLocation[0]{});

      return new FeatureContext
      {
        Document = new GherkinDocument(feature, null),
        Filename = string.Empty,
        TestSettings = null
      };
    }

    public Scenario GenerateScenarioWithNumberOfTags(int count, Func<string> tagTextGenerator)
    {
      var tags = GenerateTagsWithText(count, tagTextGenerator);

      return new Scenario(tags.ToArray(), new Location(), string.Empty, string.Empty, string.Empty, null, null);
    }

    private IEnumerable<Tag> GenerateTagsWithText(int count, Func<string> tagTextGenerator)
    {
      return Enumerable.Range(0, count)
        .Select(x => new Tag(null, tagTextGenerator()));
    }
  }
}
