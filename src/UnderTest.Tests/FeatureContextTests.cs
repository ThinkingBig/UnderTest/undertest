using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using FluentAssertions;
using Gherkin.Ast;
using NUnit.Framework;
using UnderTest.Filtering;
using UnderTest.Tests.TestSupportingClasses.FeatureContextRelated;

namespace UnderTest.Tests
{
  public class FeatureContextTests
  {
    private FeatureContextFactory featureContextFactory;

    [SetUp]
    public void Setup()
    {
      featureContextFactory = new FeatureContextFactory();
    }

    [Test]
    public void ShouldBeFiltered_WhenNoFilters_ReturnsFalse()
    {
      var instance = new FeatureContext();

      var result = instance.ShouldBeFiltered(Enumerable.Empty<TestFilter>(), new TestFilterContext());

      result.Should().Be(false);
    }

    [Test]
    public void ShouldBeFiltered_WhenOneNonApplicableFilter_ReturnsTrue()
    {
      var instance = new FeatureContext();
      var filters = new List<TestFilter>{ context => true};

      var result = instance.ShouldBeFiltered(filters, new TestFilterContext());

      result.Should().Be(true);
    }

    [Test]
    public void ShouldBeFiltered_WhenOneApplicableFilter_ReturnsTrue()
    {
      var instance = new FeatureContext();
      var filters = new List<TestFilter>{ context => false};

      var result = instance.ShouldBeFiltered(filters, new TestFilterContext());

      result.Should().Be(false);
    }

    [Test]
    [TestCase(0, 0, 0)]
    [TestCase(1, 0, 1)]
    [TestCase(0, 1, 1)]
    [TestCase(1, 1, 2)]
    public void CombineTagsWithScenario_WhenPassedTags_ReturnsResultingList(int numFeatureTags, int numScenarioTags, int resultCount)
    {
      var scenario = featureContextFactory.GenerateScenarioWithNumberOfTags(numScenarioTags, () => Guid.NewGuid().ToString());
      var instance = featureContextFactory.GenerateFeatureContextWithNumberOfTags(numFeatureTags, () => Guid.NewGuid().ToString());

      var result = instance.CombineTagsWithScenario(scenario);

      result.Count().Should().Be(resultCount);
    }

    [Test]
    public void CombineTagsWithScenario_WhenPassedDuplicateTags_ReturnsSingleTag()
    {
      const string tagText = "shoes";
      var scenario = featureContextFactory.GenerateScenarioWithNumberOfTags(1, () => tagText);
      var instance = featureContextFactory.GenerateFeatureContextWithNumberOfTags(1, () => tagText);

      var result = instance.CombineTagsWithScenario(scenario);

      result.Count().Should().Be(1);
    }
  }
}
