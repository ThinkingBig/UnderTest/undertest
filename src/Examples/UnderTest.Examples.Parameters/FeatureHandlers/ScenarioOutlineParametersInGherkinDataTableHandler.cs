using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using FluentAssertions;
using JetBrains.Annotations;
using UnderTest.Arguments;
using UnderTest.Attributes;

namespace UnderTest.Examples.Parameters.FeatureHandlers
{
  [HandlesFeature("ScenarioOutlineParametersInGherkinDataTable.feature")]
  public class ScenarioOutlineParametersInGherkinDataTableHandler : FeatureHandler
  {
    private int result;
    private int number1;
    private int number2;

    [Given("the following two numbers:")]
    public void GivenTheFollowingTwoNumbers(GherkinDataTable table)
    {
      // todo - clean this up - should be helped by https://gitlab.com/under-test/undertest/issues/117
      var row = table.Rows.First();
      number1 = Convert.ToInt32(row.Cells.First().Value);
      number2 = Convert.ToInt32(row.Cells.Skip(1).First().Value);
    }

    [When("the two numbers are added together")]
    public void WhenTheTwoNumbersAreAddedTogether()
    {
      result = number1 + number2;
    }

    [Then("the result should be <result>")]
    public void ThenResultShouldBe(int total)
    {
      result.Should().Be(total);
    }
  }
}
