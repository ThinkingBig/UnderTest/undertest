using System;
using System.Collections.Generic;
using UnderTest.Attributes;
using UnderTest.Attributes.StepParams;
using UnderTest.Examples.Parameters.StepParams;

namespace UnderTest.Examples.Parameters.FeatureHandlers
{
  [HandlesFeature("Parameters.CustomMappers.feature")]
  public class ParametersWithCustomParamMapperFeatureHandler : FeatureHandler
  {

    [Given("a user")]
    public void AUser()
    { }

    [Given("something is today")]
    public void Today([RelativeDateStepParam] DateTime today)
    {
      Log.Information($" today is: {today}");
    }

    [Given("the pet store has a(.*)")]

    public void ThePetStoreHasCollection([CollectionStepParam] List<string> petTypes)
    {
      petTypes.ForEach(Console.WriteLine);
    }

    [Then("the answers will be (.*)")]
    public void TheAnswersWillBe([CollectionStepParam(ListCollectionType = typeof(int), ItemSeparator = " - ")]
      IList<int> answers)
    {
      answers.ForEach(Console.WriteLine);
    }

    [Then("shoes is added to \"(.*)\"")]
    public void ShoesIsAddedTo([InjectedIntoStepParam] string firstWithShoes)
    {
      Console.WriteLine(firstWithShoes);
    }

    [Given(@"the user's birth date is <relative-time>")]
    public void GivenTheUsersBirthDateIsRelative([RelativeDateStepParam] DateTime birthDateP)
    {
      Log.Information($" the birthday is: {birthDateP}");
    }

    [When(@"the value '<relative-time>' is passed to a DateTime argument")]
    public void GivenTheUsersBirthDateIsToday([RelativeDateStepParam] DateTime birthDateP)
    {
      Log.Information($" the quoted birthday is: {birthDateP}");
    }

    [Then("the argument receives the appropriate relative DateTime value")]
    public void RelativeTime()
    { }
  }
}
