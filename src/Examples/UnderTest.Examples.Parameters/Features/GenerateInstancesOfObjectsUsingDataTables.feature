Feature: Generate Instances of Objects Using Data Tables

  Scenario: Generate Single instance from columns
    Given a user exists with profile information:
    | Email               | Given Name | Surname | Language Preference | Account Type |
    | charlie@example.com | Charlie    | Smith   | English             | Admin        |
    Then the profile surname should be Smith

  Scenario: Generate Single instance from rows
    Given a user exists with profile information:
      | Field                  | Value                  |
      | Given Name             | Charlie                |
      | Surname                | Smith                  |
      | Language Preference    | English                |
      | Account Type           | Admin                  |
      | Email                  | charlie@example.com    |
    Then the profile surname should be Smith

  Scenario: Multiple instances from two rows
    Given users exist with profile information:
      | Email                 | Given Name | Surname     | Language Preference | Account Type |
      | charlie@example.com   | Charlie    | Smith       | English             | Admin        |
      | oakley@example.com    | Oakley     | Johnston    | English             | User         |
    Then the last profile surname should be Johnston

  Scenario: Multiple instances many rows
    Given users exist with profile information:
      | Email                 | Given Name | Surname     | Language Preference | Account Type |
      | charlie@example.com   | Charlie    | Smith       | English             | Admin        |
      | oakley@example.com    | Oakley     | Arsenault   | English             | User         |
      | skyler@example.com    | Skyler     | Johnston    | French              | Moderator    |
    Then the last profile surname should be Johnston
