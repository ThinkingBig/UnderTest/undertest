using System.Diagnostics.CodeAnalysis;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.TestRunEventHandlers
{
  // note: be sure to review TestRunHooks
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest TestRunEventHandler Example")
          .SetProjectVersionFromAssembly(typeof(Program).Assembly))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly))
        .Execute()
        .ToExitCode();
    }
  }
}
