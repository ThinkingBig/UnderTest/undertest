﻿using System.Diagnostics.CodeAnalysis;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.ExampleTestSuite
{
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("Basic UnderTest Example")
          .SetProjectVersion("0.1.0")
          .SetProjectDescription("This is a basic example.  This test suite does not verify anything; simply executes a basic successful test."))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly))
        .Execute()
          .ToExitCode();
    }
  }
}
