using System.Threading.Tasks;
using UnderTest.Attributes;

namespace UnderTest.Examples.Basic.FeatureHandlers
{
  [HandlesFeature("SimpleFeature.feature")]
  public class SimpleFeatureHandler : FeatureHandler
  {
    [Given("Something exists")]
    public async Task SomethingExists() => Log.Information("    something exists");

    [When("Something happens")]
    public async Task SomethingHappens() => Log.Information("    something happens");

    [Then("This should now be true")]
    public async Task ThisExists() => Log.Information("    this is true");
  }
}
