using System.Threading.Tasks;
using UnderTest.Attributes;

namespace UnderTest.Examples.InconclusiveAttribute.FeatureHandlers
{
  [HandlesFeature("InconclusiveStep.feature")]
  public class InconclusiveStepFeatureHandler : FeatureHandler
  {
    [Attributes.Inconclusive(Message = "TODO: #200 - need to add functionality for this")]
    [Given("something is marked as inconclusive")]
    public void MarkedAsInconclusive() => Noop();

    [When("Something else happens")]
    public void WhenSomethingElseHappens() => Noop();

    [Then("This should be true")]
    public async Task ThenThisShouldHappen() => Noop();
  }
}
