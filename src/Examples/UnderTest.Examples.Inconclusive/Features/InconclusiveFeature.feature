Feature: Simple inconclusive feature

  # See the feature handler for how inconclusive is handled on this feature

  Scenario: Simple feature marked as inconclusive
    Given something a feature is marked as inconclusive
    When Something else happens
    Then this should be true
