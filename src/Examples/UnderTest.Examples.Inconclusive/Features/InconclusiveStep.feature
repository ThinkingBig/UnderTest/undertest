Feature: Simple inconclusive feature step

  # See the feature handler for how inconclusive is handled on this feature

  Scenario: Simple scenario with step marked as inconclusive
    Given something is marked as inconclusive
    When Something else happens
    Then this should be true
