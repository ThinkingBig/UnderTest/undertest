﻿using System;
using UnderTest.Attributes;

namespace UnderTest.Examples.ExpectedExceptions.FeatureHandlers
{
  [ExpectedFailure]
  [HandlesFeature("ExpectedFailureInAfterFeature.feature")]
  public class ExpectedFailureInAfterFeature : FeatureHandler
  {
    public override void AfterFeature(BeforeAfterFeatureContext context) => throw new Exception("after feature failed");

    [Given("Something exists")]
    public void SomethingExists() => Noop();

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
