﻿using System;
using UnderTest.Attributes;

namespace UnderTest.Examples.ExpectedExceptions.FeatureHandlers
{
  [HandlesFeature("ExpectedFailureAndStepFailsStepMarked.feature")]
  public class ExpectedFailureAndStepFailsStepMarkedFeatureHandler : FeatureHandler
  {
    [ExpectedFailure]
    [Given("Something exists")]
    public void SomethingExists() => throw new Exception("Step failed");

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
