﻿using System;
using UnderTest.Attributes;

namespace UnderTest.Examples.ExpectedExceptions.FeatureHandlers
{
  [ExpectedFailure]
  [HandlesFeature("ExpectedFailureInAfterScenario.feature")]
  public class ExpectedFailureInAfterScenario : FeatureHandler
  {
    public override void AfterScenario(BeforeAfterScenarioContext context) => throw new Exception("after scenario failed");

    [Given("Something exists")]
    public void SomethingExists() => Noop();

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
