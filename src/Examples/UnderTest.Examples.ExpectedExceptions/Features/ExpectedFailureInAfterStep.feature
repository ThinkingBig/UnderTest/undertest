Feature: Feature that is expected to fail in the AfterStep

  Scenario: Simple scenario
    Given something exists
    When something happens
    Then this should now be true
