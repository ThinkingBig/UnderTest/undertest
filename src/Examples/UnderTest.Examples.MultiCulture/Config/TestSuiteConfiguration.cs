﻿using System;
using CommandLine;
using JetBrains.Annotations;
using UnderTest.Configuration;

namespace UnderTest.Examples.MultiCulture.Config
{
  [UsedImplicitly]
  public class TestSuiteConfiguration : UnderTestCommandLineOptions
    {
      [Option('c', "culture")]
      public string Culture { get; set; } = Environment.GetEnvironmentVariable("TestSuiteName.Culture") ?? "en";
    }
}
