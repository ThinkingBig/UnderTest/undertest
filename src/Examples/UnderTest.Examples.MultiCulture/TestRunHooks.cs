﻿using System.Globalization;
using UnderTest.Attributes;
using UnderTest.Examples.MultiCulture.assets.Language;
using UnderTest.Examples.MultiCulture.Config;

namespace UnderTest.Examples.MultiCulture
{
  [TestRunEventHandler]
  public class TestRunHooks : TestRunEventHandlerBase
  {
    [PropertyInjected]
    public TestSuiteConfiguration Config { get; set; }

    public override void BeforeTestRun(BeforeAfterTestRunContext context)
    {
      // set the process culture
      CultureInfo.CurrentCulture = Hello.Culture = new CultureInfo(Config.Culture, false);

      base.BeforeTestRun(context);
    }
  }
}
