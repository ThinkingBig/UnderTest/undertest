using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnderTest.Attributes;
using UnderTest.Examples.ExecutionMode.OnlyExecuteGivens.Services;

namespace UnderTest.Examples.ExecutionMode.OnlyExecuteGivens.FeatureHandlers
{
  [HandlesFeature("SimpleFeature.feature")]
  public class SimpleFeatureHandler : FeatureHandler
  {
    [PropertyInjected]
    public SomethingSetupService SomethingService { get; set; }

    // The idea here is that since we are in ExecuteGivensOnly mode - only this given will be run
    // which will leave our data setup as it would be for the test case.
    //
    // This feature would be great for times we would want to set up data for manual or exploratory testing of scenarios.
    [Given("Something exists")]
    public void SomethingExists()
    {
      Log.Information("    Something exists");

      SomethingService.SetupSomething();
    }

    [When("Something happens")]
    public void SomethingHappens() => Log.Information("    something happens");

    [Then("This should now be true")]
    public void ThisExists() => Log.Information("    this is true");
  }
}
