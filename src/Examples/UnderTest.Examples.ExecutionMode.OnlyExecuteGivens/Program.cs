using System.Diagnostics.CodeAnalysis;
using UnderTest;
using UnderTest.Attributes;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.ExecutionMode.OnlyExecuteGivens
{ 
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest Examples Execution Mode OnlyExecuteGivens")
          .SetProjectVersionFromAssembly(typeof(GivenAttribute).Assembly))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly)
          .SetExecutionMode(UnderTest.ExecutionMode.OnlyExecuteGivens))
        .Execute()
        .ToExitCode();
    }
  }
}
