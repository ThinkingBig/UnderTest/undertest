﻿namespace UnderTest.Examples.ExecutionMode.OnlyExecuteGivens.Services
{
  public class SomethingSetupService: FeatureHandlerServiceBase
  {
    public void SetupSomething()
    {
      // this could modify a db
      //   or call an API
      //   or modify a cache
      //   etc
    }
  }
}
