using System;
using System.Diagnostics.CodeAnalysis;
using UnderTest.Reporting;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.ExecutionSettings
{
  class Program
  {
    static int Main(string[] args)
    {
      var result = new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest ExecutionSettings Examples")
          .SetProjectVersionFromAssembly(typeof(UnderTestRunner).Assembly))
        .WithTestSettings(settings => settings
          .WithExecutionSettings(e => e
            .SetRepeatInconclusiveAndFailuresPostRun(false)
            .SetScenarioTimeout(TimeSpan.FromMilliseconds(50)))
          .AddAssembly(typeof(Program).Assembly))
        .Execute();

      // we expect one failure for this test to be a success (testing wise - we want this to fail)
      return BuildExitCodeExpectingThatThisShouldFail(result);
    }

    private static int BuildExitCodeExpectingThatThisShouldFail(UnderTestRunResult result)
    {
      return (result.OverallResult == TestRunOverallResult.Failure
              && result.Scenarios.Failed == 1)
        ? (int)UnderTestExitCodes.Pass
        : (int)UnderTestExitCodes.Failure;
    }
  }
}
