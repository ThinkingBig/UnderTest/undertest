Feature: Example of ScenarioTimeout

  Scenario: Simple scenario
    Given something exists
    When something happens that will timeout - this will fail
    Then this should now be true
