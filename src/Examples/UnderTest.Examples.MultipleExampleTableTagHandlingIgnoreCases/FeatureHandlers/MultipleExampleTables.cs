using UnderTest.Attributes;

namespace UnderTest.Examples.MultipleExampleTableTagHandling.FeatureHandlers
{
  [HandlesFeature("MultipleExampleTables.feature")]
  public class MultipleExampleTablesFeatureHandler : FeatureHandler
  {
    [Given("<something> exists")]
    [When("Something happens")]
    [Then("This should now be true")]
    public void ThisShouldNowBeTrue() => Noop();
  }
}
