using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using UnderTest.Examples.MultipleExampleTableTagHandling;
using UnderTest.Reporting;

[assembly: ExcludeFromCodeCoverage]

namespace UnderTest.Examples.MultipleExampleTableTagHandlingIgnoreCases
{
  class Program
  {
    static int Main(string[] args)
    {
      var assembly = typeof(Program).Assembly;
      var result = new UnderTestRunner<TestSuiteConfiguration>()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest.Examples.MultipleExampleTableTagHandling")
          .SetProjectVersionFromAssembly(assembly))
        .WithTestSettings(settings => settings
          .AddAssembly(assembly)
          .DoNotRunTheseTags("@smoke"))
        .Execute();
      
      return HandleExpectedResultForSuccessfulSuite(result);
    }

    private static int HandleExpectedResultForSuccessfulSuite(UnderTestRunResult result)
    {
      if (result.HasAnyFailedScenarios())
      {
        return (int)UnderTestExitCodes.Failure;
      }

      try
      {
        // we have expected non-passing results from this test suite
        // handle that tracking here.
        result.Features.Passed.Should().Be(2);
        result.Scenarios.Passed.Should().Be(6);
        result.Scenarios.Skipped.Should().Be(4);

        return result.ToExitCode();
      }
      catch (Exception)
      {
        return (int)UnderTestExitCodes.Failure;
      }
    }
  }
}
