using System.Diagnostics.CodeAnalysis;
using UnderTest;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.Behavior
{
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest Behavior Example")
          .SetProjectVersionFromAssembly(typeof(UnderTestRunner).Assembly))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly))
        .Execute()
        .ToExitCode();
    }
  }
}
