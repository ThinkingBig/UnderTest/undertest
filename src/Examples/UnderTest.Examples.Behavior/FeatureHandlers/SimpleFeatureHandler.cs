using UnderTest.Attributes;
using UnderTest.Behaviors;

namespace UnderTest.Examples.Behavior.FeatureHandlers
{
  [HandlesFeature("SimpleFeature.feature")]
  public class SimpleFeatureHandler : FeatureHandler
  {
    [LogExecutionTime]
    public override void BeforeFeature(BeforeAfterFeatureContext context) => 
      Log.Information("      Before Feature");

    [LogExecutionTime]
    [LogStepExecution]
    [Given("Something exists")]
    public void SomethingExists() =>
      Log.Information("    Executing Something exists");

    [LogStepExecution]
    [When("Something happens")]
    public void SomethingHappens() =>
      Log.Information("    Executing something happens");

    [LogStepExecution]
    [Then("This should now be true")]
    public void ThisExists() =>
      Log.Information("    Executing this is true");
  }
}
