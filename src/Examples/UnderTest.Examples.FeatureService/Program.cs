using System.Diagnostics.CodeAnalysis;
using UnderTest;

[assembly: ExcludeFromCodeCoverageAttribute]

/*
note: this is a placeholder until we have a docs site.
Example Description:

After we build our first few tests for our applications, we generally start to layer additional context and complexity
within our test suites.  This complexity can lead to messy code structure.  `FeatureServices` aim to assist with
this class of problems.

FeatureServices' goal is to allow us to separate test functionality apart from our FeatureHandlers. This logic can
then be reused across multiple tests.

Within UnderTest we currently have two approaches of how we can layer FeatureServices within our tests suites:

1. FeatureHandler<T>. This approach is simple to setup:
  a. Create a class which will act as your service for the first test, Say `SimpleUnderTestService`
  b. declare your feature handler as `FeatureHandler<SimpleUnderTestService> vs `FeatureHandler`
  c. In your step calls, update the body to call the service versus executing logic in the handler

2. You can create a service like in option 1. Then inject that service into your `FeatureHandler`. This approach
allows you to use multiple services within one handler.

Note: services are live-cycled scoped to a scenario(each scenario will use a new instance).
 */

namespace UnderTest.Examples.FeatureService
{
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("Example using FeatureService to organize test logic.").
          SetProjectVersion("0.1.0"))
        .WithTestSettings(settings => settings
          .AddAssembly(typeof(Program).Assembly))
        .Execute()
          .ToExitCode();
    }
  }
}
