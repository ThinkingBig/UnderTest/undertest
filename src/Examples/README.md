# Examples

These examples exist for three purposes:

1. Showcase functionality within UnderTest at a granular level
1. Acceptance test the functionality of UnderTest itself
1. Act as a learn by example source

Current examples are:

* `UnderTest.Examples.Basic` - simple getting started test.  This test suite does not verify anything; simply executes a basic successful test.
* `UnderTest.Examples.Behaviors` - Example using behaviors that can apply aspects to steps.
* `UnderTest.Examples.ConfigureOutputSettings` - This example customizes the project's output settings.
* `UnderTest.Examples.Configuration` - Example setting up configuration for your test suite.
* `UnderTest.Examples.ExecutionMode.OnlyExecuteGivens` - Example using the ExecutionMode of OnlyExecuteGivens.
* `UnderTest.Examples.ExecutionSettings` - Example using settings within the ExecutionSettings to control the test suite.
* `UnderTest.Examples.ExpectedExceptions` - Example using `[ExpectedException]` attribute to control the failure results.
* `UnderTest.Examples.FeatureService` - Example outlining a technique to organize the logic in your test suites. 
* `UnderTest.Examples.FilterFeatures` - Example filtering features/scenarios by gherkin tags.
* `UnderTest.Examples.MultiCulture` - Example showing a recommended approach to handling multiple languages in test suites.
* `UnderTest.Examples.MultipleExampleTableTagHandling` - Shows how you can selectively execute example tables within a scenario.  
* `UnderTest.Examples.MultipleExampleTableTagHandlingIgnoreCases` - Shows how you can selectively exclude example tables within a scenario.  
* `UnderTest.Examples.Inconclusive` - Example showing the Inconclusive attribute.
* `UnderTest.Examples.Parameters` - Examples showing off how to pass parameters to steps.
* `UnderTest.Examples.ScenarioExecution` - UnderTest Example of using Scenario execution over step execution.
* `UnderTest.Examples.SeleniumStrategy` - Example using our Selenium strategy for testing against web browsers. 
* `UnderTest.Examples.TestRunEventHandlers` - Example that shoes off how to wire into global events.

