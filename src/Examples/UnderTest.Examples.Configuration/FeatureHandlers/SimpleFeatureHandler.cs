using UnderTest.Attributes;

namespace UnderTest.Examples.Configuration.FeatureHandlers
{
  [HandlesFeature("SimpleFeature.feature")]
  public class SimpleFeatureHandler : FeatureHandler
  {
    [Inject]
    public TestSuiteConfiguration Config { get; set; }

    [Given("Something exists")]
    public void SomethingExists()
    {
      Log.Information("    Something exists");
      Log.Information($"    Config value: {Config.ExampleValue}");
    }

    [When("Something happens")]
    public void SomethingHappens() => Noop();

    [Then("This should now be true")]
    public void ThisExists() => Noop();
  }
}
