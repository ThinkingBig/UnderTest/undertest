using System.Diagnostics.CodeAnalysis;
using UnderTest;

[assembly: ExcludeFromCodeCoverageAttribute]

namespace UnderTest.Examples.ScenarioExecution
{
  class Program
  {
    static int Main(string[] args)
    {
      return new UnderTestRunner<TestSuiteConfiguration>()
        .WithCommandLineArgs(args)
        .WithProjectDetails(x => x
          .SetProjectName("UnderTest Example of using Scenario execution over step execution")
          .SetProjectVersionFromAssembly(typeof(Program).Assembly))
        .WithTestSettings(settings => settings
          .ConfigureOutputSettings(output => 
            output.DisableWarnAboutScenarioExecution())
          .AddAssembly(typeof(Program).Assembly))
        .Execute()
        .ToExitCode();
    }
  }
}
