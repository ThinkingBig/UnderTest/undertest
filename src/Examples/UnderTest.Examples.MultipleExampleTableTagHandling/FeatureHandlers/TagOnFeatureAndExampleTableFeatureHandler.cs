﻿using UnderTest.Attributes;

namespace UnderTest.Examples.MultipleExampleTableTagHandling.FeatureHandlers
{
  [HandlesFeature("TagOnFeatureAndExampleTable.feature")]
  public class TagOnFeatureAndExampleTableFeatureHandler : FeatureHandler
  {
    [Step("*")]
    public void Stuff() => Noop();
  }
}
